import { TestBed, inject } from '@angular/core/testing';

import { PenjualanService } from './penjualan.service';

describe('PenjualanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PenjualanService]
    });
  });

  it('should be created', inject([PenjualanService], (service: PenjualanService) => {
    expect(service).toBeTruthy();
  }));
});
