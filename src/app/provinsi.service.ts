import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class ProvinsiService {

  	constructor(private http: Http) { }

	provinces(){
	  return this.http.get("./assets/data/provinces.json")
	    .map((response:Response)=>response.json())
	}

    regencies(){
      return this.http.get("./assets/data/regencies.json")
        .map((response:Response)=>response.json())
    }
}
