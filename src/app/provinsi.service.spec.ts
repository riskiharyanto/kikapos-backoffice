import { TestBed, inject } from '@angular/core/testing';

import { ProvinsiService } from './provinsi.service';

describe('ProvinsiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProvinsiService]
    });
  });

  it('should be created', inject([ProvinsiService], (service: ProvinsiService) => {
    expect(service).toBeTruthy();
  }));
});
