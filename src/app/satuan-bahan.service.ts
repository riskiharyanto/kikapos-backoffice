import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class SatuanBahanService {

	private url: string = "http://localhost:3001/v1/";

	constructor(private http: Http) { }

	addSatuanBahan(data){
		let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.post(this.url+"satuan",body,options)
   		.map((response:Response)=>response.json())
	}

	updateSatuanBahan(data){
		let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.put(this.url+"satuan",body,options)
   		.map((response:Response)=>response.json())
	}


	deleteSatuanBahan(data){
		let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.put(this.url+"delete/satuan",body,options)
   		.map((response:Response)=>response.json())
	}

  deleteSatuan(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
     return this.http.put(this.url+"delete/satuan",body,options)
        .map((response:Response)=>response.json())
  }

	getSatuanBahan(data){
  	return this.http.get(this.url+"satuan/"+data)
      .map((response:Response)=>response.json())
  }
}
