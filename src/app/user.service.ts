import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {
	private url: string = "http://localhost:3001/v1/";

  	constructor(private http: Http) { }

  	changeProfile(data){
	  	let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+'user',body,options)
	   		.map((response:Response)=>response.json())
  	}

  	changeAva(data){
	  	let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+'userImage',body,options)
	   		.map((response:Response)=>response.json())
  	}

  	changePassword(data){
	  	let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+'password',body,options)
	   		.map((response:Response)=>response.json())
  	}

  	uploadImageUserLocal(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
      return this.http.post(this.url+'upload/imagelocal/user',body,options)
         .map((response:Response)=>response.json())
    }

    addUser(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
      return this.http.post(this.url+'user',body,options)
         .map((response:Response)=>response.json())
    }
    
    deleteUser(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
       return this.http.put(this.url+"delete/user",body,options)
          .map((response:Response)=>response.json())
    }

    getUser(data){
    	return this.http.get(this.url+"users/"+data)
        .map((response:Response)=>response.json())
    }

}
