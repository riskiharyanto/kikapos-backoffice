import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper'; //crop image
import { NgxPaginationModule } from 'ngx-pagination'; //pagging
import { TypeaheadModule } from 'ngx-bootstrap/typeahead'; //auto search

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LoginRegisterComponent } from './login-register/login-register.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BrandComponent } from './brand/brand.component';
import { StoreComponent } from './store/store.component';
import { UserComponent } from './user/user.component';
import { SatuanBahanComponent } from './satuan-bahan/satuan-bahan.component';
import { BahanBakuComponent } from './bahan-baku/bahan-baku.component';
import { KategoriMenuComponent } from './kategori-menu/kategori-menu.component';
import { MenuComponent } from './menu/menu.component';
import { PembelianComponent } from './pembelian/pembelian.component';
import { PenjualanComponent } from './penjualan/penjualan.component';
import { LaporanPembelianComponent } from './laporan-pembelian/laporan-pembelian.component';
import { LaporanPenjualanComponent } from './laporan-penjualan/laporan-penjualan.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProfileComponent } from './profile/profile.component';


import { ProvinsiService } from './provinsi.service';
import { RegisterService } from './register.service';
import { LoginService } from './login.service';
import { LoginRegisterService } from './login-register.service';
import { BrandService } from './brand.service';
import { StoreService } from './store.service';
import { UserService } from './user.service';
import { SatuanBahanService } from './satuan-bahan.service';
import { BahanBakuService } from './bahan-baku.service';
import { KategoriMenuService } from './kategori-menu.service';
import { MenuService } from './menu.service';
import { PembelianService } from './pembelian.service';
import { PenjualanService } from './penjualan.service';
import { EqualValidator } from './equal-validator.directive';
import { AuthGuard } from './auth.guard';



const appRoutes: Routes= [ //utk routing ke masing2 component
	{
		path: 'login', 
		component: LoginComponent
	}, 
	{
		path: 'register', 
		component: RegisterComponent
	},
	{
		path: 'loginregister', 
		component: LoginRegisterComponent
	},
	{
		path: 'loginregister/:activation', 
		component: LoginRegisterComponent
	},
	{
		path: '', 
		redirectTo: '/dashboard',
		pathMatch: 'full',
		canActivate: [AuthGuard]
	},
	{
		path : 'dashboard', 
		component: DashboardComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'brand', 
		component: BrandComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'store', 
		component: StoreComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'user', 
		component: UserComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'satuanbahan', 
		component: SatuanBahanComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'bahanbaku', 
		component: BahanBakuComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'kategorimenu', 
		component: KategoriMenuComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'menu', 
		component: MenuComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'pembelian', 
		component: PembelianComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'penjualan', 
		component: PenjualanComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'laporanpembelian', 
		component: LaporanPembelianComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'laporanpenjualan', 
		component: LaporanPenjualanComponent,
		canActivate: [AuthGuard]
	},
	{
		path : 'profile', 
		component: ProfileComponent,
		canActivate: [AuthGuard]
	},
	{
		path: '**', component: PageNotFoundComponent
	}  
];

@NgModule({
 	declarations: [
	    AppComponent,
	    LoginComponent,
	    RegisterComponent,
	    EqualValidator,
	    LoginRegisterComponent,
	    ImageCropperComponent,
	    PageNotFoundComponent,
	    DashboardComponent,
	    StoreComponent,
	    UserComponent,
	    SatuanBahanComponent,
	    BahanBakuComponent,
	    KategoriMenuComponent,
	    MenuComponent,
	    PembelianComponent,
	    PenjualanComponent,
	    LaporanPembelianComponent,
	    LaporanPenjualanComponent,
	    BrandComponent,
	    ProfileComponent
	],
  	imports: [
    	BrowserModule,
    	HttpModule,
    	FormsModule,
    	NgxPaginationModule,
    	TypeaheadModule.forRoot(),
    	RouterModule.forRoot(
	      appRoutes,
	      { enableTracing: true } // <-- debugging purposes only
	    )
  	],
  	providers: [
  		ProvinsiService,
  		RegisterService,
  		LoginService, 
  		LoginRegisterService,
  		UserService,
  		BrandService,
  		StoreService,
  		SatuanBahanService,
  		BahanBakuService,
  		KategoriMenuService,
  		MenuService,
		PembelianService,
		PenjualanService,
  		AuthGuard
  	],
  	bootstrap: [AppComponent]
})
export class AppModule { }
