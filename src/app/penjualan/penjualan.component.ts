import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-penjualan',
  templateUrl: './penjualan.component.html',
  styleUrls: ['./penjualan.component.css']
})
export class PenjualanComponent implements OnInit {
  

  constructor(
  	private appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#penjualan','/penjualan');
    this.appComponent.getDataGlobal();
  }

}
