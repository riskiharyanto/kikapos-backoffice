import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class MenuService {
	
	private url: string = "http://localhost:3001/v1/";

	constructor(private http: Http) { }

	manipulateMenu(data){
		let headers = new Headers({ 'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		let body = JSON.stringify(data);
		return this.http.post(this.url+"menu", body, options)
			.map((response:Response)=>response.json())
	};

  uploadImageMenuLocal(data){
    	let headers = new Headers({ 'Content-Type': 'application/json'});
    	let options = new RequestOptions({headers: headers});
    	let body = JSON.stringify(data);
    	return this.http.post(this.url+"upload/imagelocal/menu", body, options)
      	.map((response:Response)=>response.json())
  };

  updateImageMenu(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.put(this.url+"menu/image",body,options)
       .map((response:Response)=>response.json())
  };

  deleteMenu(data){
  	let headers = new Headers({ 'Content-Type': 'application/json' });
  	let options = new RequestOptions({ headers: headers });
  	let body = JSON.stringify(data);
   	return this.http.put(this.url+"menu/delete/menu",body,options)
      	.map((response:Response)=>response.json())
  };

  deleteDetailMenu(data){
  	let headers = new Headers({ 'Content-Type': 'application/json' });
  	let options = new RequestOptions({ headers: headers });
  	let body = JSON.stringify(data);
   	return this.http.put(this.url+"menu/delete/detail",body,options)
      	.map((response:Response)=>response.json())
  };

  getMenu(data){
    	return this.http.get(this.url+"menu/"+data)
      	.map((response:Response)=>response.json())
  };
}
