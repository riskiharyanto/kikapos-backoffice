import { Component, OnInit, ViewChild } from '@angular/core';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { AppComponent } from '../app.component';
import { BrandService } from './../brand.service';
import { ProvinsiService } from './../provinsi.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  isPhotoDefault: Boolean = true;
  isButtonImportImage: Boolean = false;
  isPhotoImport: Boolean = false;
  isCity: Boolean = false;
  isSuccess: Boolean = false;
  isBrandNull: Boolean = false;
  isBrandConflict: Boolean = false;
  isSubmit: Boolean = false;
  isFirstNameBrand : Boolean = false;
  provinces =  [];
  cityes = [];
  province: any = {};
  city: any = {};
  social_media: any = {};
  contact: any = {};
  address_brand: any = {};
  model: any = {};
  dataImage : any;
  cropperSettings: CropperSettings;

  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;

  constructor(
  	private appComponent: AppComponent,
    private brandService: BrandService,
    private provinsiService: ProvinsiService ) {
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 275; //crop area width
    this.cropperSettings.height = 275; // crop area height
    this.cropperSettings.minWidth = 100; //minimal crop area width
    this.cropperSettings.minHeight = 100; //minimal crop area height
    this.cropperSettings.croppedWidth = 200; //resulting image width
    this.cropperSettings.croppedHeight = 200; //resulting image height
    this.cropperSettings.canvasWidth = 275; //canvas crop width
    this.cropperSettings.canvasHeight = 275; //canvas crop height
    this.cropperSettings.noFileInput = true;
    this.dataImage = {}; 
  }

  ngOnInit() {
    this.appComponent.changeDisplay('#brand','/brand');
    this.getData();
    this.getProvinces();
    this.appComponent.getDataGlobal();
  }

  updateBrand(){
    this.isSubmit = true;
    if (this.city.city_name == ""){
      this.isCity = false;
      this.isSubmit = false;
    } else {
      if (this.social_media.fb == "" || this.social_media.fb == undefined){
        this.social_media.fb = null;
      };
      if (this.social_media.ig == "" || this.social_media.ig == undefined){
        this.social_media.ig = null;
      };
      this.address_brand.province_id =  this.province.id;
      this.address_brand.province_name = this.province.province_name;
      this.address_brand.city_id =  this.city.id;
      this.address_brand.city_name = this.city.city_name;
      this.model.address_brand = this.address_brand;
      this.model.contact = this.contact; 
      this.model.social_media = this.social_media; 

      this.brandService.updateBrand(this.model).subscribe(
        data => {
          if( this.dataImage.image != undefined){ //update with image brand
            this.model.imagebase64 = this.dataImage.image;
            this.brandService.uploadImageBrandLocal(this.model).subscribe(
              dataImage => {
                this.model.path_image = dataImage.path;
                this.brandService.updateImageBrand(this.model).subscribe(
                  data =>{
                    this.setData(dataImage.path);
                    this.isSubmit = false;
                    this.isBrandNull= false;
                    this.isBrandConflict = false;
                    this.isSuccess = true;
                    this.dataImage = {};
                    this.defaultPhotoView();
                    setTimeout(()=>{    //<<<---    after 5seconds
                      this.isSuccess = false;
                    },5000);
                  },
                  error =>{
                    console.log(error);
                  }
                );
              }, 
              error => {
                console.log(error);
              }
            );
          } else { //update without image brand
            this.setData("");
            this.isSubmit = false;
            this.isBrandNull= false;
            this.isBrandConflict = false;
            this.isSuccess = true;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isSuccess = false;
            },5000);
          }
        }, 
        error => {
          if(error.status == 409){
            this.dataImage = {}; 
            this.isFirstNameBrand = true;
            this.model.name_brand = "";
            this.isSubmit = false;
            this.isBrandNull= false;
            this.isBrandConflict = true;
            this.isSuccess = false;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isBrandConflict = false;
            },5000);
          } else if (error.status == 404){
            this.dataImage = {}; 
            this.isSubmit = false;
            this.isBrandNull= true;
            this.isBrandConflict = false;
            this.isSuccess = false;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isBrandNull = false;
            },5000);
          }
          this.isPhotoDefault = true;
          this.isPhotoImport = false;
        }
      );
    }
  }

  getData(){ //from localStorage
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.model.id_brand= currentUser.user.id_brand;
    this.model.name_brand = currentUser.user.brand.brand_name;
    this.address_brand.detail = currentUser.user.brand.brand_address_detail;
    this.address_brand.city_id = currentUser.user.brand.brand_city_id;
    this.address_brand.city_name = currentUser.user.brand.brand_city_name;
    this.address_brand.province_id= currentUser.user.brand.brand_province_id;
    this.address_brand.province_name= currentUser.user.brand.brand_province_name;
    this.contact.email= currentUser.user.brand.brand_contact_email;
    this.contact.phone= currentUser.user.brand.brand_contact_phone;
    this.social_media.fb = currentUser.user.brand.brand_social_media_fb;
    this.social_media.ig = currentUser.user.brand.brand_social_media_ig;
    //default first load get prov and city name for validation prov and city selected
    this.province.province_id = currentUser.user.brand.brand_province_id;
    this.province.province_name = currentUser.user.brand.brand_province_name;
    this.city.id = currentUser.user.brand.brand_city_id;
    this.city.city_name = currentUser.user.brand.brand_city_name;
  }

  setData(pathImage){ //to localStorage
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
      currentUser.user.brand.brand_name = this.model.name_brand;
      currentUser.user.brand.brand_address_detail = this.address_brand.detail
      currentUser.user.brand.brand_city_id = this.address_brand.city_id;
      currentUser.user.brand.brand_city_name = this.address_brand.city_name;
      currentUser.user.brand.brand_province_id = this.address_brand.province_id;
      currentUser.user.brand.brand_province_name = this.address_brand.province_name;
      currentUser.user.brand.brand_contact_email =  this.contact.email;
      currentUser.user.brand.brand_contact_phone = this.contact.phone;
      currentUser.user.brand.brand_social_media_fb = this.social_media.fb;
      currentUser.user.brand.brand_social_media_ig = this.social_media.ig;
      if (pathImage != ""){
        currentUser.user.brand.brand_path_image = pathImage;
      }
    //replace localStorage after change dataBrand
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.appComponent.brand = currentUser.user.brand;
  }

  getProvinces(){
    this.provinsiService.provinces().subscribe(
      data =>{
        this.provinces = data;
      },
      error =>{
        this.provinces = [];
      }
    );
  }

  clickProvince(Province){
    this.isCity = true;
    this.province.id = Province.id;
    this.province.province_name = Province.province_name;
    this.city.id = "";
    this.city.city_name = "";
    this.provinsiService.regencies().subscribe(
      data =>{
        this.cityes = [];
        for (var i=0; i< data.length-1; i++){
          if (data[i].id_province == Province.id){
            this.cityes.push(data[i]);
            this.address_brand.city_id ="";
            this.address_brand.city_name ="";
            this.address_brand.province_id= "";
            this.address_brand.province_name= "";
          }
        }
      },
      error =>{
        this.cityes = [];
      }
    );
  }

  clickCity(City){
    this.city.id = City.id;
    this.city.city_name = City.city_name;
  }

  buttonImportImage(status){
    if(status){
      this.isButtonImportImage = true;
    } else {
      this.isButtonImportImage = false;
    }
  }

  changeisPhotoImport(status){
    if (status == true){
      this.isPhotoImport = true;
    } else {
      this.isPhotoImport = false;
    }
  }

  defaultisButton(){
    if (this.model.name_brand == "" && this.isFirstNameBrand  == true ){
      this.isFirstNameBrand  = false;
    }
  }

  defaultFirstNameBrand(){
    if (this.model.name_brand == "" && this.isFirstNameBrand == true){
      this.isFirstNameBrand = true;
    } else if (this.model.name_brand  != "" && this.isFirstNameBrand  == true){
      this.isFirstNameBrand = false;
    }
  }

  fileChangeListener($event) {
      var image:any = new Image();
      var file:File = $event.target.files[0];
      var myReader:FileReader = new FileReader();
      var that = this;
      myReader.onloadend = function (loadEvent:any) {
          image.src = loadEvent.target.result;
          that.cropper.setImage(image);
      };
      myReader.readAsDataURL(file);
      this.isButtonImportImage = false;
      this.isPhotoDefault = false;
      this.isPhotoImport = false;
  }

  defaultPhotoView(){
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
  }
}
