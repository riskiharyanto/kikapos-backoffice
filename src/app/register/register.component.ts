import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { RegisterService } from './../register.service';
import { BrandService } from './../brand.service';
import { ProvinsiService } from './../provinsi.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  isPhotoDefault: Boolean = true;
  isButtonImportImage: Boolean = false;
  isPhotoImport: Boolean = false;
  isLogin: Boolean;
  isPass: Boolean = false;
  isPassrequired: Boolean = false;
  isconfPass: Boolean = false;
  isButtonFirst: Boolean = false;
  isButton: Boolean = true;
  isFirstEmail: Boolean = false;
  isFirstPassword: Boolean = false;
  isFirstConfPassword: Boolean = false;
  isFirstNameBrand: Boolean = false;
  isCheckPassword: Boolean = false;
  isSubmit: Boolean = false;
  isSuccess: Boolean = false;
  isInputAgain: Boolean = false;
  isEmail: Boolean = false;
  isBrandConflict: Boolean = false;
  isRegistrationNotFound: Boolean = false;
  isCity: Boolean = false;
  provinces =  [];
  cityes = [];
  province: any = {};
  city: any = {};
	model: any = {};
  address_brand: any = {};
  contact: any = {}; 
  social_media: any = {};
  user: any = {};
  info_user: any = {};
  data: any;
  dataImage : any;
  cropperSettings: CropperSettings;

  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;

  constructor(
    private router: Router,
    private registerService: RegisterService,
    private brandService: BrandService,
    private provinsiService: ProvinsiService) { 
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 275; //crop area width
    this.cropperSettings.height = 275; // crop area height
    this.cropperSettings.minWidth = 100; //minimal crop area width
    this.cropperSettings.minHeight = 100; //minimal crop area height
    this.cropperSettings.croppedWidth = 200; //resulting image width
    this.cropperSettings.croppedHeight = 200; //resulting image height
    this.cropperSettings.canvasWidth = 275; //canvas crop width
    this.cropperSettings.canvasHeight = 275; //canvas crop height
    this.cropperSettings.noFileInput = true;
    this.dataImage = {};
  }
  

  ngOnInit() {
    /*localStorage.removeItem('loginRegister');
    localStorage.removeItem('emailRegister');*/
    this.isLogin = JSON.parse(localStorage.getItem('loginRegister'));
    this.getProvinces();
  }

  register(){
    this.isSubmit = true;
  	this.registerService.register(this.model).subscribe(
  		data => {
        this.isSuccess = true;
        this.isFirstEmail = true;
        this.isFirstPassword = true;
        this.isFirstConfPassword = true;
        this.isSubmit = false;
        this.model.email = "";
        this.model.password = "";
        this.model.confirm_password = "";
  			return true;
  		},
  		error => {
        if (error.status == 409 ){
          this.isEmail = true;
          this.model.email = "";
          this.isFirstEmail = true;
          this.isSubmit = false;
        } else if (error.status == 400){
          this.isInputAgain = true;
          this.isFirstEmail = true;
          this.isFirstPassword = true;
          this.isFirstConfPassword = true;
          this.isSubmit = false;
          this.model.email = "";
          this.model.password = "";
          this.model.confirm_password = "";
        }
      }
  	);
    this.isSuccess = false;
    this.isEmail = false;
    this.isInputAgain = false;
  }

  registerInfo(){
    //check imagebase null or not null
    //if null input brand with default path image
    //if not null 
        // upload image on directory public
        // input brand with path image 
    this.isSubmit = true;
    if (this.city.id == ""){
      this.isCity = false;
    } else {
      if (this.social_media.fb == "" || this.social_media.fb == undefined){
        this.social_media.fb = null;
      };
      if (this.social_media.ig == "" || this.social_media.ig == undefined){
        this.social_media.ig = null;
      };
      this.info_user.email = JSON.parse(localStorage.getItem('emailRegister'));
      this.user.info_user = this.info_user;
      this.address_brand.province_id =  this.province.id;
      this.address_brand.province_name = this.province.province_name;
      this.address_brand.city_id =  this.city.id;
      this.address_brand.city_name = this.city.city_name;
      this.model.address_brand = this.address_brand;
      this.model.contact = this.contact; 
      this.model.social_media = this.social_media; 
      this.model.user = this.user;

      if(this.dataImage.image == "" || this.dataImage.image == undefined){
        this.brandService.addBrand(this.model).subscribe( //input brand
          data => {
            this.model.email = JSON.parse(localStorage.getItem('emailRegister'));
            this.registerService.updateActivationRegistration(this.model).subscribe( //update Activation code on Registration
              data => {
                this.router.navigate(['/login']);
                localStorage.removeItem('loginRegister');
                localStorage.removeItem('emailRegister');
                this.isFirstNameBrand = true;
                this.model = {};
                this.address_brand = {};
                this.info_user = {};
                this.social_media = {};
                this.contact = {};
                this.isSubmit = false;
              }, 
              error => {
                console.log('error');
              }
            )
          }, 
          error => {
            if (error.status == 409){
              this.isFirstNameBrand = true;
              this.isBrandConflict = true;
              this.model.name_brand = "";
              this.isSubmit = false;
            } else if (error.status == 404){
              this.isFirstNameBrand = true;
              this.isRegistrationNotFound = true;
              this.isSubmit = false;
            };
          }
        );
        this.isBrandConflict = false;
        this.isRegistrationNotFound = false;
      } else {
        this.brandService.addBrand(this.model).subscribe(
          data =>{
            this.model.imagebase64 = this.dataImage.image;
            this.brandService.uploadImageBrandLocal(this.model).subscribe(
              dataUpload =>{
                this.model.id_brand = data.idBrand;
                this.model.path_image = dataUpload.path;
                this.brandService.updateImageBrand(this.model).subscribe( //update image brand
                  data => {
                    this.model.email = JSON.parse(localStorage.getItem('emailRegister'));
                    this.registerService.updateActivationRegistration(this.model).subscribe( //update Activation code on Registration
                      data => {
                        this.router.navigate(['/login']);
                        localStorage.removeItem('loginRegister');
                        localStorage.removeItem('emailRegister');
                        this.isFirstNameBrand = true;
                        this.model = {};
                        this.address_brand = {};
                        this.info_user = {};
                        this.social_media = {};
                        this.contact = {};
                        this.isSubmit = false;
                      }, 
                      error => {
                        console.log('error');
                      }
                    )
                  }, 
                  error => {
                    console.log('error');
                  }
                )
              },
              error =>{
                console.log(error);
              }
            )
          },
          error =>{
            if (error.status == 409){
              this.isFirstNameBrand = true;
              this.model.name_brand = "";
              this.isBrandConflict = true;
              this.isSubmit = false;
            } else if (error.status == 404){
              this.isFirstNameBrand = true;
              this.isRegistrationNotFound = true;
              this.isSubmit = false;
            };
          }
        );
        this.isBrandConflict = false;
        this.isRegistrationNotFound = false;
      }
    }
  }

  defaultisButton(){
    if (this.isButton == true){
      this.isButton = false;
    }
    if (this.model.email == "" && this.isFirstEmail == true  && this.isButton == false){
      this.isFirstEmail = false;
    }
    if (this.model.password == "" && this.isFirstPassword == true  && this.isButton == false){
      this.isFirstPassword = false;
    }
    if (this.model.confirm_password == "" && this.isFirstConfPassword == true  && this.isButton == false){
      this.isFirstConfPassword = false;
    }
  }

  defaultisFirstEmail(){
    if (this.model.email == "" && this.isFirstEmail == true){
      this.isFirstEmail = true;
    } else if (this.model.email != "" && this.isFirstEmail == true){
      this.isFirstEmail = false;
    }
  }

  defaultisFirstPassword(){
    if (this.model.password == "" && this.isFirstPassword == true){
      this.isFirstPassword = true;
    } else if (this.model.password != "" && this.isFirstPassword == true){
      this.isFirstPassword = false;
    }
  }

  defaultisFirstConfPassword(){
    if (this.model.confirm_password == "" && this.isFirstConfPassword == true){
      this.isFirstConfPassword = true;
    } else if (this.model.confirm_password != "" && this.isFirstConfPassword == true){
      this.isFirstConfPassword = false;
    }
  }

  defaultisButtonRegisterInfo(){
    if (this.model.name_brand == "" && this.isFirstNameBrand == true ){
      this.isFirstNameBrand = false;
    }
  }

  defaultisFirstNameBrand(){
    if (this.model.name_brand == "" && this.isFirstNameBrand == true){
      this.isFirstNameBrand = true;
    } else if (this.model.name_brand != "" && this.isFirstNameBrand == true){
      this.isFirstNameBrand = false;
    }
  }

  defaultisSubmit(){
    this.isSubmit = false;
  }

  defaultisconfPass(){
    if(this.model.confirm_password == "" || this.model.confirm_password == undefined){
      this.isconfPass = false;
    } else {
      this.isconfPass = true;
    }

    if(this.isPass == false){
      this.isPass = true;
    }
  }

  checkPassword(){
    if(this.model.password == "" || this.model.password == undefined){
      this.isCheckPassword = false; 
    } else {
      this.isCheckPassword = true;
    }
  }

  passrequired(){
    this.isPassrequired = true;
  }

  passnotrequired(){
    this.isPassrequired = false;
  }

  getProvinces(){
    this.provinsiService.provinces().subscribe(
      data =>{
        this.provinces = data;
      },
      error =>{
        this.provinces = [];
      }
    );
  }

  clickProvince(Province){
    this.isCity = true;
    this.province.id = Province.id;
    this.province.province_name = Province.province_name;
    this.city.id = "";
    this.city.city_name = "";
    this.provinsiService.regencies().subscribe(
      data =>{
        this.cityes = [];
        for (var i=0; i< data.length-1; i++){
          if (data[i].id_province == Province.id){
            this.cityes.push(data[i]);
          }
        }
      },
      error =>{
        this.cityes = [];
      }
    );
  }

  clickCity(City){
    this.city.id = City.id;
    this.city.city_name = City.city_name;
    this.isCity = true;
  }

  buttonImportImage(status){
    if(status){
      this.isButtonImportImage = true;
    } else {
      this.isButtonImportImage = false;
    }
  }

  changeisPhotoImport(status){
    if (status == true){
      this.isPhotoImport = true;
    } else {
      this.isPhotoImport = false;
    }
  }

  fileChangeListener($event) {
    var image:any = new Image();
    var file:File = $event.target.files[0];
    var myReader:FileReader = new FileReader();
    var that = this;
    myReader.onloadend = function (loadEvent:any) {
        image.src = loadEvent.target.result;
        that.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
    this.isPhotoDefault = false;
    this.isButtonImportImage = false;
    this.changeisPhotoImport(false);
  }
}
