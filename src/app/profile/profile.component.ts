import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from './../user.service';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	isSuccess: Boolean = false;
	isFailAva: Boolean = false;
	isFailPass: Boolean = false;
	isFailNullPass: Boolean = false;
	isConflictUsername: Boolean = false;
	isSubmit: Boolean = false;
	isFirstOldPassword : Boolean = false;
	isFirstNewPassword : Boolean = false;
	isFirstUsername : Boolean = false;
	isFirstPhone : Boolean = false;
	isChangeProfile = true;
	isButtonImportImage: Boolean = false;
	model: any ={};
	user: any ={};
	info_user: any={};
	dataImage : any;
  	cropperSettings: CropperSettings;
  	
  	@ViewChild('cropper', undefined) cropper:ImageCropperComponent;
  	constructor(
	  	private userService: UserService,
	  	private appComponent: AppComponent,
	  	private router: Router) { 
		this.cropperSettings = new CropperSettings();
	    this.cropperSettings.width = 275; //crop area width
	    this.cropperSettings.height = 275; // crop area height
	    this.cropperSettings.minWidth = 100; //minimal crop area width
	    this.cropperSettings.minHeight = 100; //minimal crop area height
	    this.cropperSettings.croppedWidth = 200; //resulting image width
	    this.cropperSettings.croppedHeight = 200; //resulting image height
	    this.cropperSettings.canvasWidth = 275; //canvas crop width
	    this.cropperSettings.canvasHeight = 275; //canvas crop height
	    this.cropperSettings.noFileInput = true;
	    this.dataImage = {};
	}

  	ngOnInit() {
	  	var currentUser = JSON.parse(localStorage.getItem('currentUser'));
	  	this.info_user.username = currentUser.user.username;
	  	this.info_user.phone = currentUser.user.phone;
	  	this.user.info_user = this.info_user;
	  	this.model.password = currentUser.user.password;
	  	this.model.email = currentUser.user.email;
	  	this.model.id_brand = currentUser.user.id_brand;
	  	this.model.iBrand = currentUser.user.iBrand;
	  	this.model.iStore = currentUser.user.iStore;
	  	this.model.iUser = currentUser.user.iUser;
	  	this.model.user = this.user;
	  	//refresh to dashboard
	  	if (this.appComponent.isChangeProfile == undefined){
	  		this.router.navigate(['/']);
	  	}
	  	this.appComponent.getDataGlobal();
  	}

  	changeProfile(){
  		this.isSubmit = true;
  		this.userService.changeProfile(this.model).subscribe(
	  		data => {
	  			this.isSubmit = false;
	  			var currentUser = JSON.parse(localStorage.getItem('currentUser'));
	  				currentUser.user.username = this.model.user.info_user.username;
	  				currentUser.user.phone = this.model.user.info_user.phone;
	  			//replace localStorage after change name and phone
	  			localStorage.setItem('currentUser', JSON.stringify(currentUser));
	  			//replace app component from info user on header app HTML
		        this.appComponent.user = currentUser.user;
		        this.isFirstUsername = true;
		        this.isFirstPhone = true;
  				this.isConflictUsername = false;
  				this.isSuccess = true;
  				setTimeout(()=>{    //<<<---    after 5seconds
	              this.isSuccess = false;
	            },5000);
	  		},
	  		error => {
	  			if(error.status == 409){
	  				this.isSubmit = false;
	  				this.isFirstUsername = true;
		        	this.isFirstPhone = true;
		        	var currentUser = JSON.parse(localStorage.getItem('currentUser'));
		        	this.model.user.info_user.username = currentUser.user.username;
		        	this.isSuccess = false;
	  				this.isConflictUsername = true;
	  				setTimeout(()=>{    //<<<---    after 5seconds
		              this.isConflictUsername = false;
		            },5000);
	  			};
	  		}
  		);
  		this.isSuccess = false;
  	}

  	changePassword(){
  		this.isSubmit = true;
  		this.userService.changePassword(this.model).subscribe(
  			data => {
  				this.isFailNullPass = false;
  				this.isFailPass = false;
  				this.isSuccess = true;
  				this.isFirstOldPassword = true;
  				this.isFirstNewPassword = true;
  				this.model.old_password = "";
  				this.model.new_password = "";
  				this.isSubmit = false;
  				setTimeout(()=>{    //<<<---    after 5seconds
					this.isSuccess = false;
				},5000);
  			}, 
  			error => {
  				if( error.status == 403){
  					this.isFirstOldPassword = true;
  					this.isFirstNewPassword = true;
  					this.isSuccess = false;
  					this.isFailNullPass = false;
  					this.isFailPass = true;
  					this.isSubmit = false;
  					setTimeout(()=>{    //<<<---    after 5seconds
					    this.isFailPass = false;
					},5000);
  				} else if (error.status = 404){
  					this.isFirstOldPassword = true;
  					this.isFirstNewPassword = true;
  					this.isSuccess = false;
  					this.isFailPass = false;
  					this.isFailNullPass = true;
  					this.isSubmit = false;
  					setTimeout(()=>{    //<<<---    after 5seconds
					    this.isFailNullPass = false;
					},5000);
  				}
  			}
  		);
  	}

  	changeAva(){
  		if(this.dataImage.image == undefined ){
  			this.isSuccess = false;
  			this.isFailAva = true;
  			setTimeout(()=>{    //<<<---    after 5seconds
			    this.isFailAva = false;
			},5000);
  		} else {
  			if (this.appComponent.isAvaDefault == true && this.appComponent.isAvaImport == false){
  				this.isSuccess = false;
  				this.isFailAva = true;
	  			setTimeout(()=>{    //<<<---    after 5seconds
				    this.isFailAva = false;
				},5000);
  			} else {
  				this.model.imagebase64 = this.dataImage.image;
	  			this.userService.uploadImageUserLocal(this.model).subscribe(
	  				data => {
	  					this.model.user.info_user.path_image = data.path;
	  					this.userService.changeAva(this.model).subscribe(
	  						data2 => {
					  			var currentUser = JSON.parse(localStorage.getItem('currentUser'));
					  				currentUser.user.path_image = data.path;
					  			//replace localStorage after change name and phone
					  			localStorage.setItem('currentUser', JSON.stringify(currentUser));
					  			//replace app component from info user on header app HTML
						        this.appComponent.user = currentUser.user;
						        this.appComponent.isAvaDefault = true;
					  			this.appComponent.isAvaImport = false;
					  			this.dataImage = {};
						        this.isSuccess = true;
					  			setTimeout(()=>{    //<<<---    after 5seconds
								    this.isSuccess = false;
								},5000);
	  						}, 
	  						error => {
	  							console.log(error);
	  						}
	  					) 
	  				}, 
	  				error => {
	  					console.log(error);
	  				}
	  			)	
  			}
  		}
  	}

  	buttonImportImage(status){
	    if(status){
	      this.isButtonImportImage = true;
	    } else {
	      this.isButtonImportImage = false;
	    }
	}

  	changeisAvaImport(status){
	    if (status == true){
	      this.appComponent.isAvaImport = true;
	    } else {
	      this.appComponent.isAvaImport = false;
	    }
	}

	defaultisSubmit(){
		this.isSubmit = false;
	}

	defaultFirstUsername(){
		if(this.model.user.info_user.username == "" && this.isFirstUsername == true){
			this.isFirstUsername = true;
		} else if (this.model.user.info_user.username != "" && this.isFirstUsername == true){
			this.isFirstUsername = false;
		};
	}

	defaultFirstPhone(){
		if(this.model.user.info_user.phone == "" && this.isFirstPhone == true){
			this.isFirstPhone = true;
		} else if (this.model.user.info_user.phone != "" && this.isFirstPhone == true){
			this.isFirstPhone = false;
		};
	}

	defaultFirstOldPassword(){
		if (this.model.old_password == "" && this.isFirstOldPassword == true){
	      this.isFirstOldPassword = true;
	    } else if (this.model.old_password  != "" && this.isFirstOldPassword  == true){
	      this.isFirstOldPassword = false;
	    }
	}

	defaultFirstNewPassword(){
		if (this.model.new_password == "" && this.isFirstNewPassword == true){
	      this.isFirstNewPassword = true;
	    } else if (this.model.new_password  != "" && this.isFirstNewPassword  == true){
	      this.isFirstNewPassword = false;
	    }
	}

	defaultisButtonProfil(){
	    if (this.model.user.info_user.username  == "" && this.isFirstUsername  == true ){
	      this.isFirstUsername  = false;
	    }
	    if (this.model.user.info_user.phone == "" && this.isFirstPhone == true){
	      this.isFirstPhone = false;
	    }
  	}

	defaultisButton(){
	    if (this.model.old_password  == "" && this.isFirstOldPassword  == true ){
	      this.isFirstOldPassword  = false;
	    }
	    if (this.model.new_password == "" && this.isFirstNewPassword == true){
	      this.isFirstNewPassword = false;
	    }
  	}

  	fileChangeListener($event) {
	    var image:any = new Image();
	    var file:File = $event.target.files[0];
	    var myReader:FileReader = new FileReader();
	    var that = this;
	    myReader.onloadend = function (loadEvent:any) {
	        image.src = loadEvent.target.result;
	        that.cropper.setImage(image);
	    };
	    myReader.readAsDataURL(file);
	    this.appComponent.isAvaDefault = false;
	    this.isButtonImportImage = false;
	    this.changeisAvaImport(false);
	}
}
