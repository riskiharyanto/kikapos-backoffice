import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  nameId: any = [ '#dashboard', '#store', '#user', '#kategorimenu', '#satuanbahan', '#bahanbaku', 
  				        '#menu', '#pembelian', '#penjualan', '#laporanpembelian', '#laporanpenjualan', '#brand'];

  constructor(
  	private appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#dashboard','/dashboard');
    this.appComponent.getDataGlobal();
    /*var currentUser = JSON.parse(localStorage.getItem('currentUser'));
      console.log(currentUser);*/
  }

}
