import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { KategoriMenuService } from './../kategori-menu.service';

@Component({
  selector: 'app-kategori-menu',
  templateUrl: './kategori-menu.component.html',
  styleUrls: ['./kategori-menu.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class KategoriMenuComponent implements OnInit {

  
  isSuccess: Boolean = false;
  isSuccessDeleteKategori: Boolean = false;
  isConflictNameKategori: Boolean = false;
  isSubmit: Boolean;
  isFirstNameKategori: Boolean;
  isEdit: Boolean;
  isAdd: Boolean;
  isPreview: Boolean;
  kategoris :any = [];
  model: any = {};
  modelDelete: any ={};

  constructor(
  	private appComponent: AppComponent,
    private kategoriMenuService: KategoriMenuService
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#kategorimenu','/kategorimenu');
    this.appComponent.getDataGlobal();
    this.getKategoris();
  }

  manipulateKategori(){
    this.isSubmit = false;
    this.model.id_brand = this.appComponent.user.id_brand;
    if(this.isAdd == true){
      this.kategoriMenuService.addKategori(this.model).subscribe(
        data =>{
          this.isConflictNameKategori = false;
          this.isSuccessDeleteKategori = false;
          this.isSuccess = true;
          this.isFirstNameKategori = true;
          this.model.id_kategori = "";
          this.model.name_kategori = "";
          this.isAdd = false;
          this.isEdit = false;
          this.isPreview = false;
          setTimeout(()=>{    //<<<---    after 5seconds
            this.isSuccess = false;
          },5000);
          this.getKategoris();
        },
        error =>{
          if (error.status == 409){
            this.isSuccess = false;
            this.isSuccessDeleteKategori = false;
            this.isConflictNameKategori = true;
            this.isFirstNameKategori = true;
            this.model.name_kategori = "";
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictNameKategori = false;
            },5000);
            this.getKategoris();
          };
        }
      )
    } else if (this.isEdit == true){
      this.kategoriMenuService.updateKategori(this.model).subscribe(
        data =>{
          this.isConflictNameKategori = false;
          this.isSuccessDeleteKategori = false;
          this.isSuccess = true;
          this.isFirstNameKategori = true;
          this.model.id_kategori = "";
          this.model.name_kategori = "";
          this.isAdd = false;
          this.isEdit = false;
          this.isPreview = false;
          setTimeout(()=>{    //<<<---    after 5seconds
            this.isSuccess = false;
          },5000);
          this.getKategoris();
        }, 
        error =>{
          if (error.status == 409){
            this.isSuccess = false;
            this.isSuccessDeleteKategori = false;
            this.isConflictNameKategori = true;
            this.isFirstNameKategori = true;
            this.model.name_kategori = "";
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictNameKategori = false;
            },5000);
            this.getKategoris();
          };
        }
      )
    };
  }

  deleteKategori(){
    this.kategoriMenuService.deleteKategori(this.modelDelete).subscribe(
      data =>{
        this.isConflictNameKategori = false;
        this.isSuccess = false;
        this.isSuccessDeleteKategori = true;
        this.isAdd = false;
        this.isEdit = false;
        this.isPreview = false;
        this.modelDelete = {};
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccessDeleteKategori = false;
        },5000);
        this.getKategoris();
      },
      error =>{
        console.log(error);
      }
    )
  };

  clickAddKategori(){
    this.model.name_kategori = "";
    this.isFirstNameKategori = true;
    this.isEdit = false;
    this.isPreview = false;
    this.isAdd = true;
  }

  clickPreviewKategori(kategori){
    this.model.name_kategori = kategori.name_kategori_menu;
    this.isAdd = false;
    this.isEdit = false;
    this.isPreview = true;
  };

  clickEditKategori(kategori){
    this.model.id_kategori = kategori.id_kategori_menu;
    this.model.name_kategori = kategori.name_kategori_menu;
    this.isAdd = false;
    this.isPreview = false;
    this.isEdit = true;
  }

  clickDeleteKategori(kategori){
    this.modelDelete.id_brand = this.appComponent.user.id_brand;
    this.modelDelete.id_kategori = kategori.id_kategori_menu;
    this.displayDeleted('#deletedBG', '#deleted', 'block');
  };

  getKategoris(){
    this.kategoris = [];
    this.kategoriMenuService.getKategori(this.appComponent.user.id_brand).subscribe(
      data =>{
        for (let i=0; i< data.result.kategori_menu.length; i++){
          if (data.result.kategori_menu[i].deleted == false){
            this.kategoris.push(data.result.kategori_menu[i]);
          }
        };
      },
      error =>{
        console.log(error);
      }
    )
  }

  defaultisFirstNameKategori(){
    if (this.model.name_kategori == "" && this.isFirstNameKategori == true){
      this.isFirstNameKategori = true;
    } else if (this.model.name_kategori != "" && this.isFirstNameKategori == true){
      this.isFirstNameKategori = false;
    }
  };

  defaultisButton(){
    if ((this.model.name_kategori == "" || this.model.name_kategori  == undefined) && this.isFirstNameKategori == true ){
      this.isFirstNameKategori = false;
    };
  };

  onClick(event) {
    console.log(event);
    if(event.target.className == 'yes'){
      this.deleteKategori();
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if ( event.target.id == 'hapus' || event.target.className == 'dialog_box'  || event.target.className == 'tittle_dialog_box' ||  
                event.target.className == 'tittle' || event.target.className == 'ask_dialog_box' || event.target.className == 'left' || 
                event.target.className == 'right' ) {
    } else {
      this.displayDeleted('#deletedBG','#deleted','none');
    };
  };

  displayDeleted(id1, id2, action){
    let div = window.document.querySelector(id1);
    div.style.display = action;
    let div2 = window.document.querySelector(id2);
    div2.style.display = action;
  };
}
