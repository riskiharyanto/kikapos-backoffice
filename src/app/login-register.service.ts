import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginRegisterService {
	private Url: string = "http://localhost:3001/v1";
	private loginRegisterUrl: string = "http://localhost:3001/v1/loginregister";
  constructor(private http: Http) { }

  loginRegister(data){
  	let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.post(this.loginRegisterUrl,body,options)
   		.map((response:Response)=>response.json())
  }

  checkActivationRegistration(data){
    return this.http.get(this.Url+'/register/'+data)
   		.map((response:Response)=>response.json())
  }

}
