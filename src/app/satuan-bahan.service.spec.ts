import { TestBed, inject } from '@angular/core/testing';

import { SatuanBahanService } from './satuan-bahan.service';

describe('SatuanBahanService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SatuanBahanService]
    });
  });

  it('should be created', inject([SatuanBahanService], (service: SatuanBahanService) => {
    expect(service).toBeTruthy();
  }));
});
