import { Component , OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class AppComponent {
  title = 'app';
  fullDate: String;
  fullTime: String;
  nameIdActive: String;
  isChangeProfile: Boolean;
  isChangePassword: Boolean;
  isChangeAva: Boolean;
  isAvaDefault: Boolean;
  isAvaImport: Boolean = false;
  isActiveChangeProfile: String;
  color: String;
  divActive: String;
  isBoxProfileShow: Boolean = false;
  brand: any = {};
  store: any = {};
  user: any = {};
  nameId: any = [ '#dashboard', '#store', '#user', '#kategorimenu', '#satuanbahan', '#bahanbaku', 
  				        '#menu', '#pembelian', '#penjualan', '#laporanpembelian', '#laporanpenjualan', '#brand'];
  constructor(
	  private router: Router
  ){}

  ngOnInit() {
    this.getDataGlobal();
    this.dateAndTime();
  };

  dateAndTime(){
    setInterval(()=>{    //<<<---    after 1seconds
      var newDate = new Date();
      var day = newDate.getDay();
      var date = newDate.getDate();
        var dt;
        if (date < 10){
          dt = "0"+date;
        } else {
          dt = date;
        };
      var month = newDate.getMonth();
      var year = newDate.getFullYear();
      var hours = newDate.getHours();
        var hr;
        if (hours < 10){
          hr = "0"+ hours;
        } else {
          hr = hours;
        };
      var minute = newDate.getMinutes();
        var mn;
        if (minute < 10){
          mn = "0"+ minute;
        }else {
          mn = minute;
        };
      var second = newDate.getSeconds();
        var sc;
        if (second < 10){
          sc = "0"+ second;
        }else {
          sc = second;
        };
      var nameDay = ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'];
      var nameMonth = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
      this.fullDate = nameDay[day]+', '+dt+' '+nameMonth[month]+' '+year;
      this.fullTime = hr+":"+mn+":"+sc;
    },1000);
  };

  getDataGlobal(){
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser != null){
      this.user = currentUser.user;
      this.brand = currentUser.user.brand;
    };
  };
  
  changeDisplay(id, route){
  	for (let i=0; i<this.nameId.length; i++){
  		if (this.nameId[i] == id){
  			let div = window.document.querySelector(id);
  			div.style.display = 'block';
  			this.divActive = id;
  		} else {
  			let div2 = window.document.querySelector(this.nameId[i]);
  			div2.style.display = 'none';
  		};
  		if (i == this.nameId.length-1){
        this.isActiveChangeProfile = "";
  		}
  	};
  }

  goTo(route){
    this.router.navigate([route]);
  }

  logout(){
    this.displayNoneForGlobal(this.divActive);
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }

  displayNoneForGlobal(id){
    if (id != ""){
      let div = window.document.querySelector(id);
      div.style.display = 'none';
    };
  }

  displayBlock(id, idNav){
  	let div = window.document.querySelector(id);
  	div.style.display = 'block';
    let divNav = window.document.querySelector(idNav);
    divNav.style.background ='#4A1757';
    divNav.style.color= '#fff';
  }

  displayNone(id, idNav){
  	if (this.divActive != id){
  		let div = window.document.querySelector(id);
  		div.style.display = 'none';
  	} 
    let divNav = window.document.querySelector(idNav);
    divNav.style.background ='#fff';
    divNav.style.color= '#4A1757';
  }

  displayAccountSettings(id, action){
    let div = window.document.querySelector(id);
    if (action == 'none'){
      div.style.display = action;
      this.isBoxProfileShow = false;
    } else {
      if (this.isBoxProfileShow == false){
        div.style.display = action;
        this.isBoxProfileShow = true;
      } else {
        div.style.display = 'none';
        this.isBoxProfileShow = false;
      }
    }
  }

  showProfile(id){
    if (id == '#changeProfile'){
      this.isChangeProfile = true;
      this.isChangePassword = false;
      this.isChangeAva = false;
      this.isAvaDefault = false;
      this.isAvaImport = false;
      this.displayNoneForGlobal(this.divActive);
    } else if (id == '#changePassword'){
      this.isChangeProfile = false;
      this.isChangePassword = true;
      this.isChangeAva = false;
      this.isAvaDefault = false;
      this.isAvaImport = false;
      this.displayNoneForGlobal(this.divActive);
    } else if (id == '#changeAva'){
      this.isChangeProfile = false;
      this.isChangePassword = false;
      this.isChangeAva = true;
      /*this.isAvaDefault = true;*/
      if (this.isActiveChangeProfile == "#changeAva" && this.isAvaDefault == false && this.isAvaImport == false){ //before save when button crop show, after import file
        this.isAvaDefault = false;
        this.isAvaImport = false;
      } else if (this.isActiveChangeProfile == "#changeAva" && this.isAvaDefault == true && this.isAvaImport == false){ //after save or firsttime imagedefault (image result save) 
        this.isAvaDefault = true;
        this.isAvaImport = false;
      } else if (this.isActiveChangeProfile == "#changeAva" && this.isAvaDefault == false && this.isAvaImport == true){ //after crop before save (image result crop)
        this.isAvaDefault = false;
        this.isAvaImport = true;
      } else {
        this.isAvaDefault = true;
        this.isAvaImport = false;
      }
      this.displayNoneForGlobal(this.divActive);
    }
    this.divActive = "";
    this.isActiveChangeProfile = id;
    this.router.navigate(['/profile']);
  }

  onClick(event) {
    if(event.target.className != 'img-ava' || event.target.className == ''){
      if (event.target.className != "menuProfile"){
        this.displayAccountSettings('#box','none');
      }
    } 
    if (event.target.className == "menuProfile"){
      this.isBoxProfileShow == false;
      this.displayAccountSettings('#box','block');
    } 
  }
}

