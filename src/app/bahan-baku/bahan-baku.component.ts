import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { BahanBakuService } from './../bahan-baku.service';
import { StoreService } from './../store.service';
import { SatuanBahanService } from './../satuan-bahan.service';

@Component({
  selector: 'app-bahan-baku',
  templateUrl: './bahan-baku.component.html',
  styleUrls: ['./bahan-baku.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class BahanBakuComponent implements OnInit {

  isSuccess: Boolean = false;
  isSuccessDeleteBahanBaku: Boolean = false;
  isConflictNameBahanBaku: Boolean = false;
  isSelectStore: Boolean;
  isAdd: Boolean;
  isPreview: Boolean;
  isEdit: Boolean;
  isSubmit: Boolean;
  isFirstNameBahan: Boolean;
  isFirstStok: Boolean;
  isFirstNameSatuan: Boolean;
  bahanbakus: any = [];
  satuans: any = [];
  stores: any = [];
  bahanBakuAndNoUrut: any = {};
  satuan: any ={};
  bahan_baku: any = {};
  model: any = {};
  modelDelete: any ={};


  constructor(
  	private appComponent: AppComponent,
    private bahanBakuService: BahanBakuService,
    private storeService: StoreService,
    private satuanBahanService: SatuanBahanService
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#bahanbaku','/bahanbaku');
    this.appComponent.getDataGlobal();
    this.getStores();
  }

  manipulateBahanBaku(){
    this.isSubmit = true;
    if (this.satuan.name_satuan == "" || this.satuan.name_satuan == undefined){
      this.isSubmit = false;
    } else {
      this.bahan_baku.satuan = this.satuan;
      this.model.bahan_baku = this.bahan_baku;
      this.model.id_brand = this.appComponent.user.id_brand;
      if(this.isAdd == true){
        this.bahanBakuService.addBahanBaku(this.model).subscribe(
          data =>{
            this.getBahanBakus(this.model.id_store);
            this.isSubmit = false;
            this.isPreview = false;
            this.isEdit = false;
            this.isAdd = false;
            this.isFirstNameBahan = true;
            this.isFirstStok = true;
            this.isFirstNameSatuan = true;
            this.isConflictNameBahanBaku = false;
            this.isSuccessDeleteBahanBaku = false;
            this.isSuccess = true;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isSuccess = false;
            },5000);
          },
          error =>{
            if(error.status == 409){
              this.isSubmit = false;
              this.bahan_baku.name_bahan_baku = "";
              this.isFirstNameBahan = true;
              this.isSuccess = false;
              this.isSuccessDeleteBahanBaku = false;
              this.isConflictNameBahanBaku = true;
              setTimeout(()=>{    //<<<---    after 5seconds
                this.isConflictNameBahanBaku = false;
              },5000);
            }
          }
        )
      } else if (this.isEdit == true){
        this.bahanBakuService.updateBahanBaku(this.model).subscribe(
          data =>{
            this.getBahanBakus(this.model.id_store);
            this.isSubmit = false;
            this.isPreview = false;
            this.isEdit = false;
            this.isAdd = false;
            this.isFirstNameBahan = true;
            this.isFirstStok = true;
            this.isFirstNameSatuan = true;
            this.isConflictNameBahanBaku = false;
            this.isSuccessDeleteBahanBaku = false;
            this.isSuccess = true;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isSuccess = false;
            },5000);
          },
          error =>{
            if(error.status == 409){
              this.isSubmit = false;
              this.bahan_baku.name_bahan_baku = "";
              this.isFirstNameBahan = true;
              this.isSuccess = false;
              this.isSuccessDeleteBahanBaku = false;
              this.isConflictNameBahanBaku = true;
              setTimeout(()=>{    //<<<---    after 5seconds
                this.isConflictNameBahanBaku = false;
              },5000);
            }
          }
        )
      }
    }
  }

  deleteBahanBaku(){
    this.bahanBakuService.deleteBahanBaku(this.modelDelete).subscribe(
      data =>{
        this.isSuccess = false;
        this.isConflictNameBahanBaku = false;
        this.isSuccessDeleteBahanBaku = true;
        this.isAdd = false;
        this.isEdit = false;;
        this.isPreview = false;
        this.getBahanBakus(this.modelDelete.id_store);
        this.modelDelete = {}; //jgn dirubah
        setTimeout(() =>{
          this.isSuccessDeleteBahanBaku = false;
        }, 5000);
      },
      error=>{
        console.log(error);
      }
    )
  };

  bahanBakuSelected(bahanBaku){
    this.bahan_baku.name_bahan_baku = bahanBaku.bahan_baku.name_bahan_baku;
    this.bahan_baku.stok = bahanBaku.bahan_baku.stok;
    this.satuan.id_satuan = bahanBaku.bahan_baku.satuan.id_satuan;
    this.satuan.name_satuan = bahanBaku.bahan_baku.satuan.name_satuan;
    this.bahan_baku.id_bahan_baku = bahanBaku.bahan_baku.id_bahan_baku;
    this.defaultisFirstSatuan();
  };

  clickAddBahanBaku(){
    if (this.model.id_store == "" || this.model.id_store == undefined){
      this.isSelectStore = true;
    } else {
      this.bahan_baku = {};
      this.satuan = {};
      this.isFirstNameBahan = true;
      this.isFirstStok = true;
      this.isFirstNameSatuan = true;
      this.isEdit = false;
      this.isPreview = false;
      this.isAdd = true;
    }
  };

  clickEditBahanBaku(bahanBaku){
    this.isAdd = false;
    this.isPreview = false;
    this.isEdit = true;
    this.isFirstNameSatuan = true;
    this.bahanBakuSelected(bahanBaku);
  };

  clickDeleteBahanBaku(bahan){
    this.modelDelete.id_brand = this.appComponent.user.id_brand;
    this.modelDelete.id_store = this.model.id_store;
    this.modelDelete.id_bahan_baku = bahan.bahan_baku.id_bahan_baku;
    this.modelDelete.name_bahan_baku = bahan.bahan_baku.name_bahan_baku;
    this.displayDeleted('#deletedBG', '#deleted', 'block');
  };

  clickPreviewBahanBaku(bahanBaku){
    this.isAdd = false;
    this.isEdit = false;
    this.isPreview = true;
    this.isFirstNameSatuan = true;
    this.bahanBakuSelected(bahanBaku);
  };

  clickStore(store){
    this.model.id_store = store.id_store;
    this.model.name_store = store.name_store;
    this.isSelectStore = false;
    this.bahan_baku = {};
    this.satuan = {};
    this.isFirstNameBahan = true;
    this.isFirstStok = true;
    this.isFirstNameSatuan = true;
    this.isAdd = false;
    this.isEdit = false;
    this.isPreview = false;
    this.getBahanBakus(store.id_store);
    this.getSatuans();
  };

  clickSatuan(satuan){
    this.isFirstNameSatuan = true;
    this.satuan.id_satuan = satuan.id_satuan;
    this.satuan.name_satuan = satuan.name_satuan;
  };
  
  getBahanBakus(idStore){
    this.bahanbakus = [];
    this.bahanBakuService.getBahanBaku(this.appComponent.user.id_brand+'/'+idStore).subscribe(
      data =>{
        let no = 0;
        for (let i=0; i<data.length; i++){
          if( data[i].bahan_baku.deleted == false){
            this.bahanBakuAndNoUrut = {};
            this.bahanBakuAndNoUrut.bahan_baku = data[i];
            this.bahanBakuAndNoUrut.nourut = no+1;
            this.bahanbakus.push(this.bahanBakuAndNoUrut);
            no+=1;
          }
        }
      }, 
      error =>{
        console.log(error);
      }
    )
  }

  getStores(){
    this.stores = [];
    this.storeService.getStore(this.appComponent.user.id_brand).subscribe(
      data => {
        for(let i=0; i<data.store.length; i++){
          if(data.store[i].id_store != 'store.XXXX' && data.store[i].deleted == false){
            this.stores.push(data.store[i]);
          }
        }
      }, 
      error => {
        console.log(error);
      }
    )
  }

  getSatuans(){
    this.satuans = [];
    this.satuanBahanService.getSatuanBahan(this.appComponent.user.id_brand).subscribe(
      data =>{
        for (let i=0; i<data.satuan.length; i++){
          if(data.satuan[i].deleted == false){
            this.satuans.push(data.satuan[i]);
          }
        }
      },
      error =>{
        console.log(error);
      }
    )
  }

  defaultisFirstNameBahan(){
    if (this.bahan_baku.name_bahan_baku == "" && this.isFirstNameBahan == true){
      this.isFirstNameBahan = true;
    } else if (this.bahan_baku.name_bahan_baku != "" && this.isFirstNameBahan == true){
      this.isFirstNameBahan = false;
    };
  };

  defaultisFirstStok(){
    if (this.bahan_baku.stok == "" && this.isFirstStok == true){
      this.isFirstStok = true;
    } else if (this.bahan_baku.stok  != "" && this.isFirstStok == true){
      this.isFirstStok = false;
    };
  };

  defaultisFirstSatuan(){
    if (this.satuan.name_satuan == "" && this.isFirstNameSatuan == true){
      this.isFirstNameSatuan = false;
    }; 
  };

  defaultisButton(){
    if ((this.bahan_baku.name_bahan_baku == "" || this.bahan_baku.name_bahan_baku == undefined) && this.isFirstNameBahan == true ){
      this.isFirstNameBahan = false;
    };
    if ((this.bahan_baku.stok == "" || this.bahan_baku.stok == undefined) && this.isFirstStok == true ){
      this.isFirstStok = false;
    };
    if ((this.satuan.name_satuan == "" || this.satuan.name_satuan  == undefined) && this.isFirstNameSatuan == true){
      this.isFirstNameSatuan = false;
    };
  };

  onClick(event) {
    if(event.target.className == 'yes'){
      this.deleteBahanBaku();
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if ( event.target.id == 'hapus' || event.target.className == 'dialog_box'  || event.target.className == 'tittle_dialog_box' ||  
                event.target.className == 'tittle' || event.target.className == 'ask_dialog_box' || event.target.className == 'left' || 
                event.target.className == 'right' ) {
    } else {
      this.displayDeleted('#deletedBG','#deleted','none');
    };
  };

  displayDeleted(id1, id2, action){
    let div = window.document.querySelector(id1);
    div.style.display = action;
    let div2 = window.document.querySelector(id2);
    div2.style.display = action;
  };
}
