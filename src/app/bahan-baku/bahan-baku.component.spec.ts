import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BahanBakuComponent } from './bahan-baku.component';

describe('BahanBakuComponent', () => {
  let component: BahanBakuComponent;
  let fixture: ComponentFixture<BahanBakuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BahanBakuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BahanBakuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
