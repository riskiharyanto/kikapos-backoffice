import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { LoginRegisterService } from './../login-register.service';
import { RegisterService } from './../register.service';

@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.css']
})
export class LoginRegisterComponent implements OnInit {
  isSubmit:Boolean = false;
  isSuccess: Boolean = false;
  isFail:Boolean = false;
  isForbiden: Boolean = false;
  model: any = {};
  
	constructor(
		private router: Router,
		private loginRegisterService: LoginRegisterService,
		private registerService: RegisterService,
    private activatedRoute: ActivatedRoute
	) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => { //cek pakai parameter atau tdk, utk sementara tanpa parameter
      let aktivasi = params['activation']; //name parameter from backend
      if (aktivasi == undefined){
        console.log('tanpa parameter');
      } else {
        this.loginRegisterService.checkActivationRegistration(aktivasi).subscribe(
          data => {
            return true;
          }, 
          error => {
            this.router.navigate(['page-not-found']);
          }
        )
      };
    });
  }

  login(){
    this.loginRegisterService.loginRegister(this.model).subscribe(
    	data => {
    		this.isSuccess = true;
    		this.registerService.login(this.model.email);
    		this.router.navigate(['/register']);
    		return true;
    	}, 
    	error => {
    		if(error.status == 403){
    			this.isForbiden = true;
    		} else if (error.status == 404){
          this.isFail = true;
    		}
    	}
    );
    this.isFail = false;
    this.isSuccess = false;
    this.isForbiden = false;
  }

}
