import { TestBed, inject } from '@angular/core/testing';

import { BahanBakuService } from './bahan-baku.service';

describe('BahanBakuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BahanBakuService]
    });
  });

  it('should be created', inject([BahanBakuService], (service: BahanBakuService) => {
    expect(service).toBeTruthy();
  }));
});
