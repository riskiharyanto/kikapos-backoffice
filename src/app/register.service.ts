import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';



@Injectable()
export class RegisterService {
  //public provinces = [];
  private url: string = "http://localhost:3001/v1/";

  constructor(private http: Http) { }

	register(data){
  	let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.post(this.url+"register",body,options)
   		.map((response:Response)=>response.json())
	}

  updateActivationRegistration(data){
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.put(this.url+"register/deleteActivation",body,options)
       .map((response:Response)=>response.json())
  }

 	login(email){
 		var status = true;
 		localStorage.setItem('loginRegister', JSON.stringify(status));
    localStorage.setItem('emailRegister', JSON.stringify(email));
	}

  provinces(){
    return this.http.get("./assets/data/provinces.json")
      .map((response:Response)=>response.json())
  }

  regencies(){
    return this.http.get("./assets/data/regencies.json")
      .map((response:Response)=>response.json())
  }

}
