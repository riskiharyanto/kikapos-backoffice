import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-laporan-penjualan',
  templateUrl: './laporan-penjualan.component.html',
  styleUrls: ['./laporan-penjualan.component.css']
})
export class LaporanPenjualanComponent implements OnInit {

  

  constructor(
  	private appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#laporanpenjualan','/laporanpenjualan');
    this.appComponent.getDataGlobal();
  }

}
