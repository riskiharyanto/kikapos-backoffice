import { TestBed, inject } from '@angular/core/testing';

import { KategoriMenuService } from './kategori-menu.service';

describe('KategoriMenuService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KategoriMenuService]
    });
  });

  it('should be created', inject([KategoriMenuService], (service: KategoriMenuService) => {
    expect(service).toBeTruthy();
  }));
});
