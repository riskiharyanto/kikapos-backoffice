import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class KategoriMenuService {

	private url: string = "http://localhost:3001/v1/";
  	
  	constructor(private http: Http) { }

  	addKategori(data){
  		let headers = new Headers({ 'Content-Type': 'application/json'});
  		let options = new RequestOptions({headers: headers});
  		let body = JSON.stringify(data);
  		return this.http.post(this.url+"kategori", body, options)
  			.map((response:Response)=>response.json())
  	}

    updateKategori(data){
      let headers = new Headers({ 'Content-Type': 'application/json'});
      let options = new RequestOptions({headers: headers});
      let body = JSON.stringify(data);
      return this.http.put(this.url+"kategori", body, options)
        .map((response:Response)=>response.json())
    }

    deleteKategori(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
       return this.http.put(this.url+"delete/kategori",body,options)
          .map((response:Response)=>response.json())
    }

    getKategori(data){
      return this.http.get(this.url+"kategori/"+data)
        .map((response:Response)=>response.json())
    }
}
