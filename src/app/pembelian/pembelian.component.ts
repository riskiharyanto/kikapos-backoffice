import { Component, OnInit } from '@angular/core';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { AppComponent } from '../app.component';
import { Router } from '@angular/router';
import { StoreService } from './../store.service';
import { BahanBakuService } from './../bahan-baku.service';
import { PembelianService } from './../pembelian.service';

@Component({
  selector: 'app-pembelian',
  templateUrl: './pembelian.component.html',
  styleUrls: ['./pembelian.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class PembelianComponent implements OnInit {

  isSuccess: Boolean = false;
  isSubmitPembelian: Boolean;
  isFirstBahanBaku: Boolean;
  isFirstHarga: Boolean;
  isFirstJumlah: Boolean;
  fullDate: String;
  fullTime: String;
  bahanbakus :any =[];
  stores: any = [];
  totalNominal = 0;
  totalItem: Number = 0;
  detailpembelians: any =[];
  brandProfile: any = {};
  storeProfile: any ={};
  addressStore: any ={};
  userProfile: any ={};
  satuan: any = {};
  detail_pembelian: any = {};
  user: any = {};
  pembelian: any = {};
  model: any = {};
  pembelianCancel: any ={};
  modelCancel: any ={};
  selectedBahanBaku: String;

  constructor(
  	private appComponent: AppComponent,
    private storeService: StoreService,
    private bahanBakuService: BahanBakuService,
    private pembelianService: PembelianService,
    private router: Router
  ) { }

 
  ngOnInit() {
    this.appComponent.changeDisplay('#pembelian','/pembelian');
    this.appComponent.getDataGlobal();
    this.getDataWhenRefresh();
    setInterval(()=>{    //<<<---    after 1seconds
      this.fullDate = this.appComponent.fullDate;
      this.fullTime = this.appComponent.fullTime;
    },1000);
  };

  transaksiPembelian(){
    this.isSubmitPembelian = true;
    if (this.selectedBahanBaku != this.detail_pembelian.name_bahan_baku){
      this.isSubmitPembelian = false;
      this.isFirstBahanBaku = false;
    } else {
      this.setTransaksiPembelian();
      this.pembelianService.addPembelian(this.model).subscribe(
        data =>{
          if(this.pembelian.id_pembelian == undefined){
            localStorage.setItem('idPembelian', data.id_pembelian);
          };
          if (data.id_pembelian != null){
            this.pembelian.id_pembelian = data.id_pembelian;
          };
          this.getDetailPembelians(this.model.id_brand, this.model.id_store, this.pembelian.id_pembelian);
          this.satuan = {};
          this.detail_pembelian ={};
          this.isFirstBahanBaku = true;
          this.isFirstHarga = true;
          this.isFirstJumlah = true;
          this.isSubmitPembelian = false;
          this.isSuccess = true;
          setTimeout(()=>{    //<<<---    after 5seconds
            this.isSuccess= false;
          },5000);
        },
        error =>{
          console.log(error);
        }
      )
    }
  };

  setTransaksiPembelian(){
    var date = new Date();
    var d = date.getDate();
      var day;
      if (d<10){
        day = "0"+d;
      }else {
        day = d;
      };
    var m = date.getMonth()+1;
      var month;
      if (m<10){
        month = "0"+m;
      } else {
        month = m;
      };
    var y = date.getFullYear();
    var hours = date.getHours();
      var h;
      if (hours<10){
        h = "0"+hours;
      }else {
        h = hours;
      };
    var minute = date.getMinutes();
      var mi;
      if (hours<10){
        mi = "0"+minute;
      }else {
        mi = minute;
      };
    var second = date.getSeconds();
      var s;
      if (second<10){
        s = "0"+second;
      }else {
        s = second;
      };
    var fullDate = y+"-"+month+"-"+day+" "+"00:00:00.000Z";
    var fullTime = y+"-"+month+"-"+day+" "+h+":"+mi+":"+s+".000Z";

    if (this.pembelian.id_pembelian != undefined){
      if (this.pembelian.id_pembelian.substr(0,8) != (day+month+y)){
        this.pembelian.id_pembelian == "";
      };
    };

    this.detail_pembelian.satuan = this.satuan;
    this.detail_pembelian.qty = parseInt(this.detail_pembelian.qty);
    this.detail_pembelian.price = parseInt(this.detail_pembelian.price);
    this.user.id_user = this.userProfile.id_user;
    this.user.username = this.userProfile.username;
    this.pembelian.detail_pembelian = this.detail_pembelian;
    this.pembelian.user = this.user;
    this.pembelian.tanggal = fullDate;
    this.pembelian.jam = fullTime;
    this.model.pembelian = this.pembelian;
  };

  closePembelian(){
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    localStorage.removeItem('openPembelian');
    localStorage.removeItem('storePembelian');
    localStorage.removeItem('idPembelian');
    this.detailpembelians = [];
    this.pembelian = {};
    this.detail_pembelian = {};
    this.satuan = {};
    this.totalItem= 0;
    this.totalNominal = 0;
    this.isFirstBahanBaku = true;
    this.isFirstHarga = true;
    this.isFirstJumlah = true;
    this.getStores();
     if (currentUser.user.id_role == 5){
       this.router.navigate(['/dashboard']);
     } else {
        this.displayPembelian('#page_pembelian','none');
     };
  };

  cancelPembelian(){
    this.pembelianService.cancelPembelian(this.modelCancel).subscribe(
      data =>{
        this.isSubmitPembelian = false;
        this.isFirstBahanBaku = true;
        this.isFirstHarga = true;
        this.isFirstJumlah = true;
        this.isFirstBahanBaku = true;
        this.isSuccess = true;
        this.satuan = {};
        this.detail_pembelian = {};
        this.pembelian = {};
        this.totalItem =0;
        this.totalNominal =0;
        this.detailpembelians = [];
        this.isSuccess = true;
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccess= false;
        },5000);
      },
      error =>{
        console.log(error);
      }
    )
  };

  cancelDetailPembelian(){
    this.pembelianService.cancelDetailPembelian(this.modelCancel).subscribe(
      data =>{
        this.isSubmitPembelian = false;
        this.isFirstBahanBaku = true;
        this.isFirstHarga = true;
        this.isFirstJumlah = true;
        this.isFirstBahanBaku = true;
        this.isSuccess = true;
        this.satuan = {};
        this.detail_pembelian = {};
        this.isSuccess = true;
        this.getDetailPembelians(this.model.id_brand, this.model.id_store, this.pembelian.id_pembelian);
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccess= false;
        },5000);
      },
      error =>{
        console.log(error);
      }
    )
  };

  getDetailPembelians(idBrand, idStore, idPembelian){
    this.detailpembelians = [];
    this.pembelianService.getPembelian(idBrand+'/'+idStore+'/'+idPembelian).subscribe(
      data =>{
        var countDetailPembelian = 0;
        for(var i=0; i<data.length; i++){
          this.detailpembelians.push(data[i].pembelian.detail_pembelian);
          countDetailPembelian+=1;
          if (i == data.length-1){
            this.totalNominal = data[i].pembelian.total_pembelian;
            this.totalItem = countDetailPembelian;
          };
        }
      },
      error =>{
        if(error.status == 404){
          this.totalNominal = 0;
          this.totalItem = 0;
        }
      }
    )
  };

  getDataWhenRefresh(){
    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.userProfile = currentUser.user;
    this.brandProfile = currentUser.user.brand;
    this.model.id_brand = currentUser.user.id_brand;
    if (currentUser.user.id_role == 5){
      this.model.id_store = currentUser.user.id_store;
      this.displayPembelian('#page_pembelian','block');
      this.getBahanBakus(this.model.id_store);
      this.addressStore.detail =  currentUser.user.store.store_address_detail;
      this.addressStore.city_name = currentUser.user.store.store_city_name;
      this.addressStore.province_name = currentUser.user.store.province_name;
      this.storeProfile.address_store = this.addressStore;
      this.storeProfile.path_image = currentUser.user.store.store_path_image;
      this.storeProfile.name_store  = currentUser.user.store.store_name;
      this.checkPhotoStore(this.storeProfile, this.brandProfile);
      var idPembelian = localStorage.getItem('idPembelian');
      this.pembelian.id_pembelian = idPembelian;
      if (this.pembelian.id_pembelian != undefined || this.pembelian.id_pembelian != ""){
        this.getDetailPembelians(this.model.id_brand, this.model.id_store, this.pembelian.id_pembelian);
      };
    }else {
      var openPembelian = localStorage.getItem('openPembelian');
      if (openPembelian == 'true'){
        var storePembelian = JSON.parse(localStorage.getItem('storePembelian'));
        var idPembelian = localStorage.getItem('idPembelian');
        this.pembelian.id_pembelian = idPembelian;
        this.model.id_store = storePembelian.store.id_store;
        this.storeProfile = storePembelian.store;
        this.checkPhotoStore(this.storeProfile, this.brandProfile);
        this.getBahanBakus(this.storeProfile.id_store);
        this.displayPembelian('#page_pembelian','block');
        if (this.pembelian.id_pembelian != undefined || this.pembelian.id_pembelian != ""){
          this.getDetailPembelians(this.model.id_brand, this.model.id_store, this.pembelian.id_pembelian);
        };
      } else if (openPembelian == undefined){
        this.getStores();
        this.addressStore.detail = "";
        this.addressStore.city_name = "";
        this.addressStore.province_name = "";
        this.storeProfile.address_store = this.addressStore;
        this.storeProfile.path_image = "";
        this.storeProfile.name_store  = "";
      };
    };
  };

  checkPhotoStore(store, brand){
    if (this.storeProfile.path_image == 'http://localhost:3001/v1/public/image_upload/image_brand/defaultBrand.jpg'){
      this.storeProfile.path_image = this.brandProfile.brand_path_image;
    }
  };

  getStores(){
    this.stores = [];
    this.storeService.getStore(this.appComponent.user.id_brand).subscribe(
      data => {
        for(let i=0; i<data.store.length; i++){
          if(data.store[i].id_store != 'store.XXXX' && data.store[i].deleted == false){
            this.stores.push(data.store[i]);
          }
        }
      }, 
      error => {
        console.log(error);
      }
    )
  };

  getBahanBakus(idStore){
    this.bahanbakus = [];
    this.bahanBakuService.getBahanBaku(this.appComponent.user.id_brand+'/'+idStore).subscribe(
      data =>{
        for (let i=0; i<data.length; i++){
          if( data[i].bahan_baku.deleted == false){
            this.bahanbakus.push(data[i]);
          }
        }
      }, 
      error =>{
        console.log(error);
      }
    )
  };

  clickStore(store){
    this.model.id_pembelian = 
    this.model.id_store = store.id_store;
    this.getBahanBakus(this.model.id_store);
    this.storeProfile = store;
    localStorage.setItem('openPembelian','true');
    localStorage.setItem('storePembelian', JSON.stringify({  store: store }));
    this.displayPembelian('#page_pembelian','block');
    this.checkPhotoStore(this.storeProfile, this.brandProfile);
  };

  clickClosePembelian(){
    this.displayCancel('#deletedBG', '#deleted', 'block');
    this.displayComponentCancel('#ask1', '#ask2', '#ask3', '#yes1', '#yes2', '#yes3', 'none', 'none', 'block');
  };

  clickNewTransaksi(){
    this.pembelian = {};
    this.detail_pembelian = {};
    this.satuan = {};
    this.isFirstBahanBaku = true;
    this.isFirstHarga = true;
    this.isFirstJumlah = true;
    this.isSubmitPembelian = false;
    localStorage.removeItem('idPembelian');
    this.detailpembelians = [];
    this.totalItem = 0;
    this.totalNominal = 0;
  };

  clickCancelPembelian(){
    if (this.pembelian.id_pembelian == undefined || this.pembelian.id_pembelian == ""){
    } else {
      this.pembelianCancel.id_pembelian = this.pembelian.id_pembelian;
      this.modelCancel.id_brand = this.model.id_brand;
      this.modelCancel.id_store = this.model.id_store;
      this.modelCancel.pembelian = this.pembelianCancel;
      this.displayCancel('#deletedBG', '#deleted', 'block');
      this.displayComponentCancel('#ask1', '#ask2', '#ask3', '#yes1', '#yes2', '#yes3', 'block', 'none', 'none');
    };
  };

  clickCancelDetailPembelian(detailPembelian){
    this.modelCancel.id_brand = this.model.id_brand;
    this.modelCancel.id_store = this.model.id_store;
    this.pembelianCancel.id_pembelian = this.pembelian.id_pembelian;
    this.pembelianCancel.id_bahan_baku = detailPembelian.id_bahan_baku;
    this.modelCancel.pembelian = this.pembelianCancel;
    this.displayCancel('#deletedBG', '#deleted', 'block');
    this.displayComponentCancel('#ask1', '#ask2', '#ask3', '#yes1', '#yes2', '#yes3', 'none', 'block', 'none');
  };

  defaultisFirstJumlah(){
    if ((this.detail_pembelian.qty == "" || this.detail_pembelian.qty  == undefined)&& this.isFirstJumlah == true){
      this.isFirstJumlah = true;
    } else if (this.detail_pembelian.qty != "" && this.isFirstJumlah == true){
      this.isFirstJumlah = false;
    };
  };

  defaultisFirstHarga(){
    if ((this.detail_pembelian.price == "" || this.detail_pembelian.price  == undefined)&& this.isFirstHarga == true){
      this.isFirstHarga = true;
    } else if (this.detail_pembelian.price != "" && this.isFirstHarga == true){
      this.isFirstHarga = false;
    }
  };
  
  defaultisButton(){
    if ((this.detail_pembelian.name_bahan_baku == "" || this.detail_pembelian.name_bahan_baku  == undefined) && this.isFirstBahanBaku == true ){
      this.isFirstBahanBaku = false;
    };
    if ((this.detail_pembelian.qty == "" || this.detail_pembelian.qty  == undefined) && this.isFirstJumlah == true ){
      this.isFirstJumlah = false;
    };
    if ((this.detail_pembelian.price == "" || this.detail_pembelian.price  == undefined) && this.isFirstHarga == true ){
      this.isFirstHarga = false;
    };
  };

  displayPembelian(id, action){
    let div = window.document.querySelector(id);
    div.style.display = action;
  };

  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.isFirstBahanBaku = true;
    this.detail_pembelian.id_bahan_baku = e.item.bahan_baku.id_bahan_baku;
    this.detail_pembelian.name_bahan_baku = e.item.bahan_baku.name_bahan_baku;
    this.satuan.id_satuan = e.item.bahan_baku.satuan.id_satuan;
    this.satuan.name_satuan = e.item.bahan_baku.satuan.name_satuan;
    this.selectedBahanBaku = e.item.bahan_baku.name_bahan_baku;
  };

  onClick(event){
    if(event.target.id == 'yes1'){
      this.cancelPembelian();
      this.displayCancel('#deletedBG','#deleted','none');
    } else if (event.target.id == 'yes2'){
      this.cancelDetailPembelian();
      this.displayCancel('#deletedBG','#deleted','none');
    } else if (event.target.id == 'yes3'){
      this.closePembelian();
      this.displayCancel('#deletedBG','#deleted','none');
    } else if (event.target.id == 'tutup') {
      this.displayCancel('#deletedBG','#deleted','none');
    } else if ( event.target.id == 'hapus' || event.target.className == 'dialog_box'  || event.target.className == 'tittle_dialog_box' ||  
                event.target.className == 'tittle' || event.target.className == 'ask_dialog_box' ||  event.target.id == 'ask1' ||
                event.target.id == 'ask2' || event.target.className == 'left' || event.target.className == 'yes' || 
                event.target.className == 'right' ){
    } else {
      this.displayCancel('#deletedBG','#deleted','none');
    };
  };

  displayCancel(id1, id2, action){
    let div = window.document.querySelector(id1);
    div.style.display = action;
    let div2 = window.document.querySelector(id2);
    div2.style.display = action;
  };

  displayComponentCancel(idAskPembelian, idAskDetPembelian, idAskClose, idYesPembelian, idYesDetPembelian, idYesClose, action1, action2, action3){
    let div = window.document.querySelector(idAskPembelian);
    div.style.display = action1;
    let div2 = window.document.querySelector(idAskDetPembelian);
    div2.style.display = action2;
    let div3 = window.document.querySelector(idAskClose);
    div3.style.display = action3;

    let div4 = window.document.querySelector(idYesPembelian);
    div4.style.display = action1;
    let div5 = window.document.querySelector(idYesDetPembelian);
    div5.style.display = action2;
    let div6 = window.document.querySelector(idYesClose);
    div6.style.display = action3;
  };
};
