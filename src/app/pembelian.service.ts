import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class PembelianService {

	private url: string = "http://localhost:3001/v1/";

  	constructor( private http: Http ) { }

  	addPembelian(data){
  		let headers = new Headers({ 'Content-Type': 'application/json'});
		let options = new RequestOptions({headers: headers});
		let body = JSON.stringify(data);
		return this.http.post(this.url+"pembelian", body, options)
			.map((response:Response)=>response.json())
  	};

  	cancelPembelian(data){
  		let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+"pembelian/cancel",body,options)
	       .map((response:Response)=>response.json())
  	};

  	cancelDetailPembelian(data){
  		let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+"pembelian/detail/cancel",body,options)
	       .map((response:Response)=>response.json())
  	};

  	getPembelian(data){
  		return this.http.get(this.url+"pembelian/"+data)
      	.map((response:Response)=>response.json())
  	};
}
