import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { StoreService } from './../store.service';
import { UserService } from './../user.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class UserComponent implements OnInit {

  isSuccess: Boolean = false;
  isSuccessDeleteUser: Boolean = false;
  isConflictEmail: Boolean = false;
  isConflictUsername: Boolean = false;
  isSuperUser: Boolean;
  isManager: Boolean;
  isAdmin: Boolean ;
  isSupervisor: Boolean ;
  isLogistic: Boolean ;
  isKasir: Boolean;
  isServer: Boolean;
  isSubmit: Boolean;
  isSelectStore: Boolean;
  isAdd: Boolean;
  isPreview: Boolean;
  isFirstUsername: Boolean;
  isFirstEmail: Boolean;
  isFirstPassword: Boolean;
  isFirstPhone: Boolean;
  passwordDefault: string = '12345';
  divIdSubNavActive: string;
  stores: any = [];
  users: any = [];
  role: any = {};
  info_user: any = {};
  user: any = {};
  model: any = {};
  modelDelete: any ={};
  nameRoot = ['isSuperUser','isManager','isAdmin','isSupervisor','isLogistic','isKasir','isServer'];
  nameId: any = [ '#userSubSuperUser', '#userSubManager', '#userSubAdmin', '#userSubSupervisor', '#userSubLogistic', '#userSubKasir', '#userSubServer'];
  constructor(
  	private appComponent: AppComponent,
    private storeService: StoreService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#user','/user');
    this.appComponent.getDataGlobal();
  }

  addUser(){
    this.isSubmit = true;
    this.info_user.role = this.role;
    this.user.info_user = this.info_user;
    this.model.user = this.user;
    this.model.id_brand = this.appComponent.user.id_brand;
    this.userService.addUser(this.model).subscribe(
      data => {
        this.getUsers(this.model.id_store, this.role.id_role);
        this.isSubmit = false;
        this.isPreview = false;
        this.isAdd = false;
        this.isFirstEmail = true;
        this.isFirstPassword = true;
        this.isFirstPhone = true;
        this.isFirstUsername = true;
        this.isConflictEmail = false;
        this.isConflictUsername = false;
        this.isSuccessDeleteUser = false;
        this.isSuccess = true;
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccess = false;
        },5000);
      },
      error =>{
        if(error.status == 409){
          var errorStatus = JSON.parse(error._body);
          if (errorStatus.status == "Email Conflict"){
            this.isSubmit = false;
            this.info_user.email = "";
            this.isFirstEmail = true;
            this.isSuccess = false;
            this.isConflictUsername = false;
            this.isSuccessDeleteUser = false;
            this.isConflictEmail = true;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictEmail = false;
            },5000);
          } else if (errorStatus.status == "Username Conflict"){
            this.isSubmit = false;
            this.info_user.username = "";
            this.isFirstUsername = true;
            this.isSuccess = false;
            this.isConflictEmail = false;
            this.isSuccessDeleteUser = false;
            this.isConflictUsername = true;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictUsername = false;
            },5000);
          }
          
        }
      }
    )
  }

  deleteUser(){
    this.userService.deleteUser(this.modelDelete).subscribe(
      data =>{
        this.isAdd = false;
        this.isPreview = false;
        this.modelDelete = {};
        this.getUsers(this.model.id_store, this.role.id_role);
        this.isSuccess = false;
        this.isConflictEmail = false;
        this.isConflictUsername = false;
        this.isSuccessDeleteUser = true;
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccessDeleteUser = false;
        },5000);
      },
      error =>{
        console.log(error);
      }
    )
  }

  activeSubNav(id,root){
    for (let i=0; i<this.nameId.length; i++){
      if (this.nameId[i] == id){
        let div = window.document.querySelector(id);
        div.style.background ='#4A1757';
        div.style.color= '#fff';
        this.divIdSubNavActive = this.nameId[i];
      } else {
        let div2 = window.document.querySelector(this.nameId[i]);
        div2.style.background ='#fff';
        div2.style.color= '#4A1757';
      };
    };
    if (root == 'isSuperUser'){
      this.isSuperUser = true;
      this.isManager = false;
      this.isAdmin = false;
      this.isSupervisor = false;
      this.isLogistic = false;
      this.isKasir = false;
      this.isServer = false;
      this.getUsers('store.XXXX', 1);
      this.role.id_role = 1;
      this.role.name_role = '';
      this.model.id_store = 'store.XXXX';
      this.model.name_store = 'Head Store';
    } else if (root == 'isManager'){
      this.isSuperUser = false;
      this.isManager = true;
      this.isAdmin = false;
      this.isSupervisor = false;
      this.isLogistic = false;
      this.isKasir = false;
      this.isServer = false;
      this.getUsers('store.XXXX', 2);
      this.role.id_role = 2;
      this.role.name_role = '';
      this.model.id_store = 'store.XXXX';
      this.model.name_store = 'Head Store';
    } else if (root == 'isAdmin'){
      this.isSuperUser = false;
      this.isManager = false;
      this.isAdmin = true;
      this.isSupervisor = false;
      this.isLogistic = false;
      this.isKasir = false;
      this.isServer = false;
      this.role.id_role = 3;
      this.role.name_role = '';
      this.model.id_store = '';
      this.model.name_store = '';
    } else if (root == 'isSupervisor'){
      this.isSuperUser = false;
      this.isManager = false;
      this.isAdmin = false;
      this.isSupervisor = true;
      this.isLogistic = false;
      this.isKasir = false;
      this.isServer = false;
      this.role.id_role = 4;
      this.role.name_role = '';
      this.model.id_store = '';
      this.model.name_store = '';
    } else if (root == 'isLogistic'){
      this.isSuperUser = false;
      this.isManager = false;
      this.isAdmin = false;
      this.isSupervisor = false;
      this.isLogistic = true;
      this.isKasir = false;
      this.isServer = false;
      this.role.id_role = 5;
      this.role.name_role = '';
      this.model.id_store = '';
      this.model.name_store = '';
    } else if (root == 'isKasir'){
      this.isSuperUser = false;
      this.isManager = false;
      this.isAdmin = false;
      this.isSupervisor = false;
      this.isLogistic = false;
      this.isKasir = true;
      this.isServer = false;
      this.role.id_role = 6;
      this.role.name_role = '';
      this.model.id_store = '';
      this.model.name_store = '';
    } else if (root == 'isServer'){
      this.isSuperUser = false;
      this.isManager = false;
      this.isAdmin = false;
      this.isSupervisor = false;
      this.isLogistic = false;
      this.isKasir = false;
      this.isServer = true;
      this.role.id_role = 7;
      this.role.name_role = '';
      this.model.id_store = '';
      this.model.name_store = '';
    }
    this.info_user = {};
    this.isFirstEmail = true;
    this.isFirstPassword = true;
    this.isFirstPhone = true;
    this.isFirstUsername = true;
    this.isAdd = false;
    this.isPreview = false;
    this.isSelectStore = false;
    this.users = [];
    this.getStores();
  }

  offSubNav(id){
    if (id != this.divIdSubNavActive){
      let div = window.document.querySelector(id);
      div.style.background ='#fff';
      div.style.color= '#4A1757';
    }
  }

  onSubNav(id){
    let div = window.document.querySelector(id);
    div.style.background ='#4A1757';
    div.style.color= '#fff';
  }

  getStores(){
    this.stores = [];
    this.storeService.getStore(this.appComponent.user.id_brand).subscribe(
      data => {
        for(let i=0; i<data.store.length; i++){
          if(data.store[i].id_store != 'store.XXXX' && data.store[i].deleted == false){
            this.stores.push(data.store[i]);
          }
        }
      }, 
      error => {
        console.log(error);
      }
    )
  }

  clickStore(store, role){
    this.getUsers(store.id_store, role);
    this.isAdd = false;
    this.model.id_store = store.id_store;
    this.model.name_store = store.name_store;
    this.isSelectStore = false;
    this.info_user = {};
    this.isFirstEmail = true;
    this.isFirstPassword = true;
    this.isFirstPhone = true;
    this.isFirstUsername = true;
    this.isAdd = false;
    this.isPreview = false;
  };
  
  clickAddUser(){
    if (this.model.id_store == "" || this.model.id_store == undefined){
      this.isSelectStore = true;
    } else {
      this.info_user = {};
      this.isPreview = false;
      this.isAdd = true;
    }
  };

  clickDeleteUser(user){
    this.modelDelete.email = user.email;
    this.displayDeleted('#deletedBG', '#deleted', 'block');
  };

  clickPreviewUser(user){
    this.info_user.email = user.info_user.email;
    this.info_user.password = user.info_user.password;
    this.info_user.username = user.info_user.username;
    this.info_user.phone = user.info_user.phone;
    this.info_user.path_image = user.info_user.path_image;
    this.isAdd = false;
    this.isPreview = true;
  };

  getUsers(store, role){
    this.users = [];
    this.userService.getUser(this.appComponent.user.id_brand).subscribe(
      data =>{
        for (let i=0; i< data[0].store.length; i++){
          if(data[0].store[i].id_store == store){
            for (let j=0; j<data[0].store[i].user.length; j++){
              if(data[0].store[i].user[j].info_user.role.id_role == role && data[0].store[i].user[j].info_user.deleted == false){
                this.users.push(data[0].store[i].user[j]);
              };
            };
          };
        };
      },
      error =>{  
        console.log(error);
      }
    );
  };

  defaultisFirstUsername(){
    if (this.info_user.username == "" && this.isFirstUsername == true){
      this.isFirstUsername = true;
    } else if (this.info_user.username != "" && this.isFirstUsername == true){
      this.isFirstUsername = false;
    }
  };

  defaultisFirstEmail(){
    if (this.info_user.email == "" && this.isFirstEmail == true){
      this.isFirstEmail = true;
    } else if (this.info_user.email  != "" && this.isFirstEmail == true){
      this.isFirstEmail = false;
    }
  };

  defaultisFirstPassword(){
    if (this.info_user.password == "" && this.isFirstPassword == true){
      this.isFirstPassword = true;
    } else if (this.info_user.password  != "" && this.isFirstPassword == true){
      this.isFirstPassword = false;
    }
  };

  defaultisFirstPhone(){
    if (this.info_user.phone == "" && this.isFirstPhone == true){
      this.isFirstPhone = true;
    } else if (this.info_user.phone  != "" && this.isFirstPhone == true){
      this.isFirstPhone = false;
    }
  };

  defaultisButton(){
    if ((this.info_user.username == "" || this.info_user.username == undefined) && this.isFirstUsername == true ){
      this.isFirstUsername = false;
    };
    if ((this.info_user.email == "" || this.info_user.email == undefined) && this.isFirstEmail == true){
      this.isFirstEmail = false;
    };
    if ((this.info_user.password == "" || this.info_user.password== undefined) && this.isFirstPassword == true){
      this.isFirstPassword = false;
    };
    if ((this.info_user.phone  == "" || this.info_user.phone == undefined) && this.isFirstPhone == true){
      this.isFirstPhone = false;
    };
  };


  onClick(event) {
    if(event.target.className == 'yes'){
      this.deleteUser();
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if ( event.target.id == 'hapus' || event.target.className == 'dialog_box'  || event.target.className == 'tittle_dialog_box' ||  
                event.target.className == 'tittle' || event.target.className == 'ask_dialog_box' || event.target.className == 'left' || 
                event.target.className == 'right' ) {
    } else {
      this.displayDeleted('#deletedBG','#deleted','none');
    };
  };

  displayDeleted(id1, id2, action){
    let div = window.document.querySelector(id1);
    div.style.display = action;
    let div2 = window.document.querySelector(id2);
    div2.style.display = action;
  };
}
