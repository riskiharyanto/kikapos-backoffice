import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SatuanBahanComponent } from './satuan-bahan.component';

describe('SatuanBahanComponent', () => {
  let component: SatuanBahanComponent;
  let fixture: ComponentFixture<SatuanBahanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SatuanBahanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SatuanBahanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
