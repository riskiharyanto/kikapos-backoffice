import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { SatuanBahanService } from './../satuan-bahan.service';

@Component({
  selector: 'app-satuan-bahan',
  templateUrl: './satuan-bahan.component.html',
  styleUrls: ['./satuan-bahan.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class SatuanBahanComponent implements OnInit {

  isSuccess: Boolean = false;
  isSuccessDeleteSatuan: Boolean = false;
  isConflictNameSatuan: Boolean = false;
  isSubmit: Boolean;
  isFirstNameSatuan: Boolean;
  isEdit: Boolean;
  isAdd: Boolean;
  isPreview: Boolean;
  satuans :any = [];
  model: any = {};
  modelDelete: any ={};

  constructor(
  	private appComponent: AppComponent,
    private satuanBahanService: SatuanBahanService
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#satuanbahan','/satuanbahan');
    this.appComponent.getDataGlobal();
    this.getSatuans();
  }

  manipulateSatuan(){
    this.isSubmit = false;
    this.model.id_brand = this.appComponent.user.id_brand;
    if(this.isAdd == true){
      this.satuanBahanService.addSatuanBahan(this.model).subscribe(
        data =>{
          this.isConflictNameSatuan = false;
          this.isSuccessDeleteSatuan = false;
          this.isSuccess = true;
          this.isFirstNameSatuan = true;
          this.model.id_satuan = "";
          this.model.name_satuan = "";
          this.isAdd = false;
          this.isEdit = false;
          this.isPreview = false;
          setTimeout(()=>{    //<<<---    after 5seconds
            this.isSuccess = false;
          },5000);
          this.getSatuans();
        },
        error =>{
          if (error.status == 409){
            this.isSuccess = false;
            this.isSuccessDeleteSatuan = false;
            this.isConflictNameSatuan = true;
            this.isFirstNameSatuan = true;
            this.model.name_satuan = "";
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictNameSatuan = false;
            },5000);
            this.getSatuans();
          };
        }
      )
    } else if (this.isEdit == true){
      this.satuanBahanService.updateSatuanBahan(this.model).subscribe(
        data =>{
          this.isConflictNameSatuan = false;
          this.isSuccessDeleteSatuan = false;
          this.isSuccess = true;
          this.isFirstNameSatuan = true;
          this.model.id_satuan = "";
          this.model.name_satuan = "";
          this.isAdd = false;
          this.isEdit = false;
          this.isPreview = false;
          setTimeout(()=>{    //<<<---    after 5seconds
            this.isSuccess = false;
          },5000);
          this.getSatuans();
        }, 
        error =>{
          if (error.status == 409){
            this.isSuccess = false;
            this.isSuccessDeleteSatuan = false;
            this.isConflictNameSatuan = true;
            this.isFirstNameSatuan = true;
            this.model.name_satuan = "";
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictNameSatuan = false;
            },5000);
            this.getSatuans();
          };
        }
      )
    };
  };

  deleteSatuan(){
    this.satuanBahanService.deleteSatuan(this.modelDelete).subscribe(
      data =>{
        this.isSuccess = false;
        this.isConflictNameSatuan = false;
        this.isSuccessDeleteSatuan = true;
        this.isAdd = false;
        this.isPreview = false;
        this.isEdit = false;
        this.modelDelete = {};
        this.getSatuans();
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccessDeleteSatuan = false;
        },5000);
      },
      error =>{
        console.log(error);
      }
    )
  };

  clickAddSatuan(){
    this.isFirstNameSatuan = true;
    this.model.name_satuan = "";
    this.isEdit = false;
    this.isPreview = false;
    this.isAdd = true;
  }

  clickEditSatuan(satuan){
    this.model.id_satuan = satuan.id_satuan;
    this.model.name_satuan = satuan.name_satuan;
    this.isAdd = false;
    this.isPreview = false;
    this.isEdit = true;
  };

  clickDeleteSatuan(satuan){
    this.modelDelete.id_brand = this.appComponent.user.id_brand;
    this.modelDelete.id_satuan = satuan.id_satuan;
    this.displayDeleted('#deletedBG', '#deleted', 'block');
  };

  clickPreviewSatuan(satuan){
    this.model.name_satuan = satuan.name_satuan;
    this.isAdd = false;
    this.isEdit = false;
    this.isPreview = true;
  };

  getSatuans(){
    this.satuans = [];
    this.satuanBahanService.getSatuanBahan(this.appComponent.user.id_brand).subscribe(
      data =>{
        for (let i=0; i< data.satuan.length; i++){
          if (data.satuan[i].deleted == false){
            this.satuans.push(data.satuan[i]);
          }
        };
      },
      error =>{
        console.log(error);
      }
    )
  }

  defaultisFirstNameSatuan(){
    if (this.model.name_satuan == "" && this.isFirstNameSatuan == true){
      this.isFirstNameSatuan = true;
    } else if (this.model.name_satuan != "" && this.isFirstNameSatuan == true){
      this.isFirstNameSatuan = false;
    }
  };

  defaultisButton(){
    if ((this.model.name_satuan == "" || this.model.name_satuan  == undefined) && this.isFirstNameSatuan == true ){
      this.isFirstNameSatuan = false;
    };
  }

  onClick(event) {
    if(event.target.className == 'yes'){
      this.deleteSatuan();
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if ( event.target.id == 'hapus' || event.target.className == 'dialog_box'  || event.target.className == 'tittle_dialog_box' ||  
                event.target.className == 'tittle' || event.target.className == 'ask_dialog_box' || event.target.className == 'left' || 
                event.target.className == 'right' ) {
    } else {
      this.displayDeleted('#deletedBG','#deleted','none');
    };
  };

  displayDeleted(id1, id2, action){
    let div = window.document.querySelector(id1);
    div.style.display = action;
    let div2 = window.document.querySelector(id2);
    div2.style.display = action;
  };

}
