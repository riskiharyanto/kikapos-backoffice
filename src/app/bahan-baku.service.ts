import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class BahanBakuService {

	private url: string = "http://localhost:3001/v1/";

  	constructor(private http: Http) { }

  	addBahanBaku(data){
  		let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.post(this.url+"bahanbaku",body,options)
	   		.map((response:Response)=>response.json())
  	}

  	updateBahanBaku(data){
  		let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+"bahanbaku",body,options)
	   		.map((response:Response)=>response.json())
  	}

  	deleteBahanBaku(data){
  		let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+"delete/bahanbaku",body,options)
	   		.map((response:Response)=>response.json())
  	}

  	getBahanBaku(data){
	    return this.http.get(this.url+"bahanbaku/"+data)
	   		.map((response:Response)=>response.json())
  	}

}		
