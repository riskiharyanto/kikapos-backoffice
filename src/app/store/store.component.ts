import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { AppComponent } from '../app.component';
import { ProvinsiService } from './../provinsi.service';
import { StoreService } from './../store.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class StoreComponent implements OnInit {
  
  isSuccess: Boolean = false;
  isSuccessDeleteStore: Boolean = false;
  isConflictName: Boolean = false;
  isAdd: Boolean;
  isEdit: Boolean;
  isPreview: Boolean;
  isInputShow: Boolean = false;
  isPhotoDefault: Boolean = true;
  isButtonImportImage: Boolean = false;
  isPhotoImport: Boolean = false;
  isFirstNameStore: Boolean = false;
  isFirstAlamatStore: Boolean = false;
  isFirstEmailStore: Boolean = false;
  isFirstPhoneStore: Boolean = false;
  isFirstProvinsiStore: Boolean = false;
  isFirstCityStore: Boolean = false;
  isSubmit: Boolean = false;
  provinces =  [];
  cityes = [];
  province: any = {};
  city: any = {};
  stores: any = [];
  storeAndNoUrut : any = {}; 
  social_media_store: any = {};
  contact_store: any = {};
  address_store: any = {};
  model: any = {};
  modelDelete: any ={};
  dataImage : any;
  cropperSettings: CropperSettings;

  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;

  constructor(
  	private appComponent: AppComponent,
    private storeService: StoreService,
    private activatedRoute: ActivatedRoute,
    private provinsiService: ProvinsiService ) { 
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 275; //crop area width
    this.cropperSettings.height = 275; // crop area height
    this.cropperSettings.minWidth = 100; //minimal crop area width
    this.cropperSettings.minHeight = 100; //minimal crop area height
    this.cropperSettings.croppedWidth = 200; //resulting image width
    this.cropperSettings.croppedHeight = 200; //resulting image height
    this.cropperSettings.canvasWidth = 275; //canvas crop width
    this.cropperSettings.canvasHeight = 275; //canvas crop height
    this.cropperSettings.noFileInput = true;
    this.dataImage = {}; 
  }

  ngOnInit() {
    this.appComponent.changeDisplay('#store','/store');
    this.appComponent.getDataGlobal();
    this.getStores();
    this.getProvinces();
  }

  storeSelected(store){
    this.address_store.city_id = store.address_store.city_id;
    this.address_store.city_name = store.address_store.city_name;
    this.address_store.province_id = store.address_store.province_id;
    this.address_store.province_name = store.address_store.province_name;
    this.address_store.detail = store.address_store.detail;
    if (store.social_media_store.fb == null){
      this.social_media_store.fb = "";
    } else {
      this.social_media_store.fb = store.social_media_store.fb;
    };
    if (store.social_media_store.ig == null){
      this.social_media_store.ig = "";
    } else {
      this.social_media_store.ig = store.social_media_store.ig; 
    };
    this.contact_store.phone_store = store.contact_store.phone_store;
    this.contact_store.email_store = store.contact_store.email_store;
    this.model.name_store = store.name_store;
    this.model.path_image = store.path_image;
    this.model.id_store = store.id_store;
    this.defaultisFirstProvinsiStore();
    this.defaultisFirstCityStore();
  }

  getStores(){
    this.stores = [];
    this.storeService.getStore(this.appComponent.user.id_brand).subscribe(
      data => {
        let no = 0;
        for(let i=0; i<data.store.length; i++){
          if (data.store[i].id_store != 'store.XXXX' && data.store[i].deleted == false){
            this.storeAndNoUrut = {};
            this.storeAndNoUrut.store = data.store[i];
            this.storeAndNoUrut.nourut = no+1;
            this.stores.push(this.storeAndNoUrut);
            no+=1;
          };
        };
      }, 
      error => {
        console.log(error);
      }
    )
  }

  manipulateStore(){
    this.isSubmit = true;
    if (this.city.id == "" || this.province.id == ""){
      this.isSubmit = false;
    }else {
      if (this.social_media_store.fb == "" || this.social_media_store.fb == undefined){
        this.social_media_store.fb = null;
      };
      if (this.social_media_store.ig == "" || this.social_media_store.ig == undefined){
        this.social_media_store.ig = null;
      };
      this.address_store.province_id =  this.province.id;
      this.address_store.province_name = this.province.province_name;
      this.address_store.city_id =  this.city.id;
      this.address_store.city_name = this.city.city_name;
      this.model.address_store = this.address_store;
      this.model.social_media_store = this.social_media_store;
      this.model.contact_store = this.contact_store;
      this.model.id_brand = this.appComponent.user.id_brand;

      if (this.isAdd == true){
        this.storeService.addStore(this.model).subscribe(
          dataStore =>{
            if (this.dataImage.image != undefined){
              this.model.imagebase64 = this.dataImage.image;
              this.storeService.uploadImageStoreLocal(this.model).subscribe(
                dataImage =>{
                  this.model.path_image = dataImage.path;
                  this.model.id_store = dataStore.id_store;
                  this.storeService.updateImageStore(this.model).subscribe(
                    data =>{
                      this.dataImage = {};
                      this.isSubmit = false;
                      this.isFirstNameStore= true;
                      this.isFirstAlamatStore = true;
                      this.isFirstEmailStore = true;
                      this.isFirstPhoneStore = true;
                      this.isAdd = false;
                      this.isEdit = false;
                      this.isPreview = false;
                      this.isConflictName = false;
                      this.isSuccessDeleteStore = false;
                      this.isSuccess = true;
                      this.getStores();
                      setTimeout(()=>{    //<<<---    after 5seconds
                        this.isSuccess = false;
                      },5000);
                    },
                    error =>{
                      console.log(error);
                    }
                  );
                }, 
                error =>{
                  console.log(error);
                }
              );
            } else {
              this.isConflictName = false;
              this.isSuccessDeleteStore = false;
              this.isSuccess = true;
              this.dataImage = {};
              this.isSubmit = false;
              this.isFirstNameStore= true;
              this.isFirstAlamatStore = true;
              this.isFirstEmailStore = true;
              this.isFirstPhoneStore = true;
              this.isAdd = false;
              this.isEdit = false;
              this.isPreview = false;
              this.getStores();
              setTimeout(()=>{    //<<<---    after 5seconds
                this.isSuccess = false;
              },5000);
            }
          },
          error =>{
            if(error.status == 409){
              this.isSubmit = false;
              this.model.name_store = "";
              this.isFirstNameStore= true;
              this.isSuccess = false;
              this.isSuccessDeleteStore = false;
              this.isConflictName = true;
              setTimeout(()=>{    //<<<---    after 5seconds
                this.isConflictName = false;
              },5000);
            }
          }
        );
      } else if (this.isEdit == true){
        this.storeService.updateStore(this.model).subscribe(
          data =>{
            if (this.dataImage.image != undefined){
              this.model.imagebase64 = this.dataImage.image;
              this.storeService.uploadImageStoreLocal(this.model).subscribe(
                dataImage =>{
                  this.model.path_image = dataImage.path;
                  this.storeService.updateImageStore(this.model).subscribe(
                    data =>{
                      this.dataImage = {};
                      this.isSubmit = false;
                      this.isFirstNameStore= true;
                      this.isFirstAlamatStore = true;
                      this.isFirstEmailStore = true;
                      this.isFirstPhoneStore = true;
                      this.isAdd = false;
                      this.isEdit = false;
                      this.isPreview = false;
                      this.isConflictName = false;
                      this.isSuccessDeleteStore = false;
                      this.isSuccess = true;
                      this.getStores();
                      setTimeout(()=>{    //<<<---    after 5seconds
                        this.isSuccess = false;
                      },5000);
                    },
                    error =>{
                      console.log(error);
                    }
                  );
                }, 
                error =>{
                  console.log(error);
                }
              );
            } else {
              this.isConflictName = false;
              this.isSuccessDeleteStore = false;
              this.isSuccess = true;
              this.dataImage = {};
              this.isSubmit = false;
              this.isFirstNameStore= true;
              this.isFirstAlamatStore = true;
              this.isFirstEmailStore = true;
              this.isFirstPhoneStore = true;
              this.isAdd = false;
              this.isEdit = false;
              this.isPreview = false;
              this.getStores();
              setTimeout(()=>{    //<<<---    after 5seconds
                this.isSuccess = false;
              },5000);
            }
          },
          error =>{
            if(error.status == 409){
              this.isSubmit = false;
              this.model.name_store = "";
              this.isFirstNameStore= true;
              this.isSuccess = false;
              this.isSuccessDeleteStore = false;
              this.isConflictName = true;
              setTimeout(()=>{    //<<<---    after 5seconds
                this.isConflictName = false;
              },5000);
            }
          }
        );
      }
    }
  }

  deleteStore(){
    this.storeService.deleteStore(this.modelDelete).subscribe(
      data =>{
        this.isSuccess = false;
        this.isConflictName = false;
        this.isSuccessDeleteStore = true;
        this.isAdd = false;
        this.isPreview = false;
        this.isEdit = false;
        this.modelDelete = {};
        this.getStores();
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccessDeleteStore = false;
        },5000);
      },
      error =>{
        console.log(error);
      }
    )
  }

  clickAddStore(){
    this.province.id = "";
    this.province.province_name = "";
    this.city.id = "";
    this.city.city_name = "";
    this.social_media_store = {};
    this.contact_store = {};
    this.address_store = {};
    this.model = {};
    this.isInputShow = true;
    this.isEdit = false;
    this.isPreview = false;
    this.isAdd = true;
    this.isFirstNameStore= true;
    this.isFirstAlamatStore = true;
    this.isFirstEmailStore = true;
    this.isFirstPhoneStore = true;
    this.isFirstProvinsiStore = true;
    this.isFirstCityStore= true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.dataImage = {};
  }

  clickEditStore(store){
    this.province.id = store.address_store.province_id;
    this.province.province_name = store.address_store.province_name;
    this.city.id = store.address_store.city_id;
    this.city.city_name = store.address_store.city_name;
    this.isInputShow = true;
    this.isAdd = false;
    this.isPreview = false;
    this.isEdit = true;
    this.isFirstProvinsiStore = true;
    this.isFirstCityStore= true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.dataImage = {};
    this.storeSelected(store);
  }

  clickDeleteStore(store){
    this.modelDelete.id_brand = this.appComponent.user.id_brand;
    this.modelDelete.id_store = store.id_store;
    this.displayDeleted('#deletedBG', '#deleted', 'block');
  };

  clickPreviewStore(store){
    this.province.id = store.address_store.province_id;
    this.province.province_name = store.address_store.province_name;
    this.city.id = store.address_store.city_id;
    this.city.city_name = store.address_store.city_name;
    this.isEdit = false;
    this.isAdd = false;
    this.isPreview = true;
    this.isFirstProvinsiStore = true;
    this.isFirstCityStore= true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.dataImage = {};
    this.storeSelected(store);
  }

  getProvinces(){
    this.provinsiService.provinces().subscribe(
      data =>{
        this.provinces = data;
      },
      error =>{
        this.provinces = [];
      }
    );
  }

  clickProvince(Province){
    this.isFirstProvinsiStore = true;
    this.isFirstCityStore = true;
    this.province.id = Province.id;
    this.province.province_name = Province.province_name;
    this.city.id = "";
    this.city.city_name = "";
    this.provinsiService.regencies().subscribe(
      data =>{
        this.cityes = [];
        for (var i=0; i< data.length-1; i++){
          if (data[i].id_province == Province.id){
            this.cityes.push(data[i]);
            /*this.address_store.city_id ="";
            this.address_store.city_name ="";
            this.address_store.province_id= "";
            this.address_store.province_name= "";*/
          }
        }
      },
      error =>{
        this.cityes = [];
      }
    );
  }

  clickCity(City){
    this.isFirstCityStore = true;
    this.city.id = City.id;
    this.city.city_name = City.city_name;
  }

  defaultisFirstNameStore(){
    if (this.model.name_store == "" && this.isFirstNameStore == true){
      this.isFirstNameStore = true;
    } else if (this.model.name_store != "" && this.isFirstNameStore == true){
      this.isFirstNameStore = false;
    }
  }

  defaultisFirstAlamatStore(){
    if (this.address_store.detail == "" && this.isFirstAlamatStore == true){
      this.isFirstAlamatStore = true;
    } else if (this.address_store.detail != "" && this.isFirstAlamatStore == true){
      this.isFirstAlamatStore = false;
    }
  }

  defaultisFirstEmailStore(){
    if (this.contact_store.email == "" && this.isFirstEmailStore == true){
      this.isFirstEmailStore = true;
    } else if (this.contact_store.email != "" && this.isFirstEmailStore == true){
      this.isFirstEmailStore = false;
    }
  }

  defaultisFirstPhoneStore(){
    if (this.contact_store.phone == "" && this.isFirstPhoneStore == true){
      this.isFirstPhoneStore = true;
    } else if (this.contact_store.phone != "" && this.isFirstPhoneStore == true){
      this.isFirstPhoneStore = false;
    }
  }

  defaultisFirstProvinsiStore(){
    if (this.province.province_name == "" && this.isFirstProvinsiStore == true){
      this.isFirstProvinsiStore = false;
    } 
  }

  defaultisFirstCityStore(){
    if (this.city.city_name == "" && this.isFirstCityStore == true){
      this.isFirstCityStore = false;
    } 
  }

  defaultisButton(){
    if ((this.model.name_store == "" || this.model.name_store == undefined) && this.isFirstNameStore == true ){
      this.isFirstNameStore = false;
    };
    if ((this.address_store.detail == "" || this.address_store.detail == undefined) && this.isFirstAlamatStore == true){
      this.isFirstAlamatStore = false;
    };
    if ((this.contact_store.email == "" || this.contact_store.email == undefined) && this.isFirstEmailStore == true){
      this.isFirstEmailStore = false;
    };
    if ((this.contact_store.phone == "" || this.contact_store.phone == undefined) && this.isFirstPhoneStore == true){
      this.isFirstPhoneStore = false;
    };
    if ((this.province.province_name  == "" || this.province.province_name  == undefined) && this.isFirstProvinsiStore == true){
      this.isFirstProvinsiStore = false;
    };
    if ((this.city.city_name  == "" || this.city.city_name  == undefined) && this.isFirstCityStore == true){
      this.isFirstCityStore = false;
    };
  }

  buttonImportImage(status){
    if(status){
      this.isButtonImportImage = true;
    } else {
      this.isButtonImportImage = false;
    }
  }

  changeisPhotoImport(status){
    if (status == true){
      this.isPhotoImport = true;
    } else {
      this.isPhotoImport = false;
    }
  }

  fileChangeListener($event) {
      var image:any = new Image();
      var file:File = $event.target.files[0];
      var myReader:FileReader = new FileReader();
      var that = this;
      myReader.onloadend = function (loadEvent:any) {
          image.src = loadEvent.target.result;
          that.cropper.setImage(image);
      };
      myReader.readAsDataURL(file);
      this.isButtonImportImage = false;
      this.isPhotoDefault = false;
      this.changeisPhotoImport(false);
  }

  onClick(event) {
    if(event.target.className == 'yes'){
      this.deleteStore();
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if ( event.target.id == 'hapus' || event.target.className == 'dialog_box'  || event.target.className == 'tittle_dialog_box' ||  
                event.target.className == 'tittle' || event.target.className == 'ask_dialog_box' || event.target.className == 'left' || 
                event.target.className == 'right' ) {
    } else {
      this.displayDeleted('#deletedBG','#deleted','none');
    };
  };

  displayDeleted(id1, id2, action){
    let div = window.document.querySelector(id1);
    div.style.display = action;
    let div2 = window.document.querySelector(id2);
    div2.style.display = action;
  };
}
