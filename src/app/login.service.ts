import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class LoginService {
	private loginUrl: string = "http://localhost:3001/v1/login";
  constructor( private http: Http ) { }

  login(data){
  	let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let body = JSON.stringify(data);
    return this.http.post(this.loginUrl,body,options)
   		.map((response:Response)=>response.json())
  }

}
