import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanPembelianComponent } from './laporan-pembelian.component';

describe('LaporanPembelianComponent', () => {
  let component: LaporanPembelianComponent;
  let fixture: ComponentFixture<LaporanPembelianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanPembelianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanPembelianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
