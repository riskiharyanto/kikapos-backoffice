import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-laporan-pembelian',
  templateUrl: './laporan-pembelian.component.html',
  styleUrls: ['./laporan-pembelian.component.css']
})
export class LaporanPembelianComponent implements OnInit {

  

  constructor(
  	private appComponent: AppComponent
  ) { }

  ngOnInit() {
    this.appComponent.changeDisplay('#laporanpembelian','/laporanpembelian');
    this.appComponent.getDataGlobal();
  }

}
