import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class StoreService {

	private url: string = "http://localhost:3001/v1/";

  	constructor(private http: Http) { }

  	addStore(data){
  		let headers = new Headers({ 'Content-Type': 'application/json'});
  		let options = new RequestOptions({headers: headers});
  		let body = JSON.stringify(data);
  		return this.http.put(this.url+"addStore", body, options)
  			.map((response:Response)=>response.json())
  	}

    uploadImageStoreLocal(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
      return this.http.post(this.url+"upload/imagelocal/store",body,options)
         .map((response:Response)=>response.json())
    }

    updateStore(data){
      let headers = new Headers({ 'Content-Type': 'application/json'});
      let options = new RequestOptions({headers: headers});
      let body = JSON.stringify(data);
      return this.http.put(this.url+"updateStore", body, options)
        .map((response:Response)=>response.json())
    }

    updateImageStore(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
      return this.http.put(this.url+"updateImageStore",body,options)
         .map((response:Response)=>response.json())
    }

    deleteStore(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
       return this.http.put(this.url+"delete/store",body,options)
          .map((response:Response)=>response.json())
    }

    getStore(data){
      return this.http.get(this.url+"stores/"+data)
        .map((response:Response)=>response.json())
    }

}
