import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';
import { ImageCropperComponent, CropperSettings } from 'ng2-img-cropper';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { AppComponent } from '../app.component';
import { StoreService } from './../store.service';
import { KategoriMenuService } from './../kategori-menu.service';
import { BahanBakuService } from './../bahan-baku.service';
import { MenuService } from './../menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
  host: { '(document:click)': 'onClick($event)'}
})
export class MenuComponent implements OnInit {
  isSuccessMenu: Boolean = false;
  isSuccessDeleteMenu: Boolean = false;
  isSuccessDeleteDetailMenu: Boolean = false;
  isSuccessDetailMenu: Boolean = false;
  isConflictMenu: Boolean = false;
  isConflictDetailMenu: Boolean = false;
  isSubmitMenu: Boolean;
  isSubmitDetailMenu: Boolean;
  isSelectStore: Boolean;
  isAddMenu: Boolean;
  isEditMenu: Boolean;
  isPreviewMenu: Boolean;
  isShowDetailMenu:Boolean;
  isAddDetailMenu: Boolean;
  isEditDetailMenu: Boolean;
  isPreviewDetailMenu: Boolean;
  isPhotoDefault: Boolean = true;
  isButtonImportImage: Boolean = false;
  isPhotoImport: Boolean = false;
  isFirstNameMenu: Boolean;
  isFirstPriceMenu: Boolean;
  isFirstKategoriMenu: Boolean;
  isFirstBahanBaku: Boolean;
  isFirstJumlahBahanBaku: Boolean;
  stores: any = [];
  kategoris: any = [];
  bahanbakus :any =[];
  menus: any =[];
  detailmenus: any =[];
  menuAndNoUrut: any = {};
  detailMenuAndNoUrut: any = {};
  tempMenuSelected: any = {};
  satuan :any = {};
  detail_menu :any = {};
  kategori: any = {};
  menu :any = {};
  store: any = {};
  model: any = {};
  modelDelete: any ={};
  dataImage : any = {};
  selectedBahanBaku: String;
  cropperSettings: CropperSettings;

  @ViewChild('cropper', undefined) cropper:ImageCropperComponent;

  constructor(
  	private appComponent: AppComponent,
    private storeService: StoreService,
    private kategoriMenuService: KategoriMenuService,
    private bahanBakuService: BahanBakuService,
    private menuService: MenuService ) {
      this.cropperSettings = new CropperSettings();
      this.cropperSettings.width = 275; //crop area width
      this.cropperSettings.height = 275; // crop area height
      this.cropperSettings.minWidth = 100; //minimal crop area width
      this.cropperSettings.minHeight = 100; //minimal crop area height
      this.cropperSettings.croppedWidth = 200; //resulting image width
      this.cropperSettings.croppedHeight = 200; //resulting image height
      this.cropperSettings.canvasWidth = 275; //canvas crop width
      this.cropperSettings.canvasHeight = 275; //canvas crop height
      this.cropperSettings.noFileInput = true;
      this.dataImage = {}; 
    }

  ngOnInit() {
    this.appComponent.changeDisplay('#menu','/menu');
    this.appComponent.getDataGlobal();
    this.getStores();
    this.getKategoris();
  };

  manipulateMenu(){
    this.isSubmitMenu = true;
    if(this.kategori.name_kategori_menu == "" || this.kategori.name_kategori_menu == undefined){
      this.isSubmitMenu = false;
    } else {
      this.setManipulateMenu();
      this.menuService.manipulateMenu(this.model).subscribe(
        dataManipulate =>{
          if (this.dataImage.image != undefined){
            this.model.imagebase64 = this.dataImage.image;
            this.menuService.uploadImageMenuLocal(this.model).subscribe(
              dataimage =>{
                this.model.path_image = dataimage.path;
                this.model.id_store = this.store.id_store;
                this.menuService.updateImageMenu(this.model).subscribe(
                  data =>{
                    this.dataImage = {};
                    this.isSubmitMenu = false;
                    this.isFirstNameMenu = true;
                    this.isFirstPriceMenu = true;
                    this.isFirstKategoriMenu = true;
                    this.isFirstBahanBaku = true;
                    this.isFirstJumlahBahanBaku = true;
                    this.isAddMenu = false;
                    this.isEditMenu = false;
                    this.isPreviewMenu = false;
                    this.isAddDetailMenu = false;
                    this.isEditDetailMenu = false;
                    this.isPreviewDetailMenu = false;
                    this.isSuccessDetailMenu = false;
                    this.isConflictDetailMenu = false;
                    this.isConflictMenu = false;
                    this.isSuccessDeleteMenu = false;
                    this.isSuccessDeleteDetailMenu = false;
                    this.isSuccessMenu = true;
                    this.getMenus(this.store.id_store);
                    setTimeout(()=>{    //<<<---    after 5seconds
                      this.isSuccessMenu = false;
                    },5000);
                  }, 
                  error =>{
                    console.log(error);
                  }
                )
              },
              error =>{
                console.log(error);
              }
            )
          } else {
            this.dataImage = {};
            this.isSubmitMenu = false;
            this.isFirstNameMenu = true;
            this.isFirstPriceMenu = true;
            this.isFirstKategoriMenu = true;
            this.isFirstBahanBaku = true;
            this.isFirstJumlahBahanBaku = true;
            this.isAddMenu = false;
            this.isEditMenu = false;
            this.isPreviewMenu = false;
            this.isAddDetailMenu = false;
            this.isEditDetailMenu = false;
            this.isPreviewDetailMenu = false;
            this.isSuccessDetailMenu = false;
            this.isConflictDetailMenu = false;
            this.isConflictMenu = false;
            this.isSuccessDeleteMenu = false;
            this.isSuccessDeleteDetailMenu = false;
            this.isSuccessMenu = true;
            this.getMenus(this.store.id_store);
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isSuccessMenu = false;
            },5000);
          }
        }, 
        error =>{
          if(error.status == 409){
            this.isSubmitMenu = false;
            this.menu.name_menu = "";
            this.isFirstNameMenu = true;
            this.isSuccessMenu = false;
            this.isSuccessDetailMenu = false;
            this.isConflictDetailMenu = false;
            this.isSuccessDeleteMenu = false;
            this.isSuccessDeleteDetailMenu = false;
            this.isConflictMenu = true;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictMenu = false;
            },5000);
          }
        }
      )
    }
  };

  manipulateDetailMenu(){
    this.isSubmitDetailMenu = true;
    if (this.selectedBahanBaku != this.detail_menu.name_bahan_baku){
      this.isSubmitDetailMenu = false;
      this.isFirstBahanBaku = false;
    } else {
      this.setManipulateDetailMenu();
      this.menuService.manipulateMenu(this.model).subscribe(
        data =>{
          this.dataImage = {};
          this.isSubmitDetailMenu = false;
          this.isFirstNameMenu = true;
          this.isFirstPriceMenu = true;
          this.isFirstKategoriMenu = true;
          this.isFirstBahanBaku = true;
          this.isFirstJumlahBahanBaku = true;
          this.isAddMenu = false;
          this.isEditMenu = false;
          this.isPreviewMenu = true;
          this.isAddDetailMenu = false;
          this.isEditDetailMenu = false;
          this.isPreviewDetailMenu = false;
          this.isConflictDetailMenu = false;
          this.isConflictMenu = false;
          this.isSuccessMenu = false;
          this.isSuccessDeleteMenu = false;
          this.isSuccessDeleteDetailMenu = false;
          this.isSuccessDetailMenu = true;
          this.getMenus(this.store.id_store);
          setTimeout(()=>{    //<<<---    after 5seconds
            this.isSuccessDetailMenu = false;
          },5000);
        },
        error =>{
          if(error.status == 409){
            this.isSubmitDetailMenu = false;
            this.detail_menu.name_bahan_baku = "";
            this.isFirstBahanBaku = true;
            this.isSuccessMenu = false;
            this.isSuccessDetailMenu = false;
            this.isConflictMenu = false;
            this.isSuccessDeleteMenu = false;
            this.isSuccessDeleteDetailMenu = false;
            this.isConflictDetailMenu = true;
            setTimeout(()=>{    //<<<---    after 5seconds
              this.isConflictDetailMenu = false;
            },5000);
          }
        }
      )
    };
  };

  deleteMenu(){
    this.menuService.deleteMenu(this.modelDelete).subscribe(
      data =>{
        this.isSuccessDetailMenu = false;
        this.isConflictDetailMenu = false;
        this.isConflictMenu = false;
        this.isSuccessMenu = false;
        this.isSuccessDeleteDetailMenu = false;
        this.isSuccessDeleteMenu = true;
        this.modelDelete = {};
        this.isAddMenu= false;
        this.isEditMenu= false;
        this.isPreviewMenu= false;
        this.isShowDetailMenu = false;
        this.getMenus(this.store.id_store);
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccessDeleteMenu = false;
        },5000);
      }, 
      error =>{
        console.log(error);
      }
    )
  };

  deleteDetailMenu(){
    this.menuService.deleteDetailMenu(this.modelDelete).subscribe(
      data =>{
        this.isSuccessDetailMenu = false;
        this.isConflictDetailMenu = false;
        this.isConflictMenu = false;
        this.isSuccessMenu = false;
        this.isSuccessDeleteMenu = false;
        this.isSuccessDeleteDetailMenu = true;
        this.modelDelete = {};
        this.isShowDetailMenu = true;
        this.isAddDetailMenu= false;
        this.isEditDetailMenu= false;
        this.isPreviewDetailMenu= false;
        this.getMenus(this.store.id_store);
        setTimeout(()=>{    //<<<---    after 5seconds
          this.isSuccessDeleteDetailMenu = false;
        },5000);
      }, 
      error =>{
        console.log(error);
      }
    )
  };

  setManipulateMenu(){
    if (this.isAddMenu == true){
      this.menu.id_menu = "";
    } else {
      this.model.id_menu = this.menu.id_menu;
    }
    this.satuan.id_satuan = 'first';
    this.satuan.name_satuan = 'first';
    this.detail_menu.id_bahan_baku = 'first';
    this.detail_menu.name_bahan_baku = 'first';
    this.detail_menu.jumlah = 0;
    this.detail_menu.satuan = this.satuan;
    this.menu.kategori = this.kategori;
    this.menu.detail_menu = this.detail_menu;
    this.store.menu = this.menu;
    this.model.id_brand = this.appComponent.user.id_brand;
    this.model.store = this.store;
  };

  setManipulateDetailMenu(){
    this.detail_menu.satuan = this.satuan;
    this.menu.kategori = this.kategori;
    this.menu.detail_menu = this.detail_menu;
    this.store.menu = this.menu;
    this.model.id_brand = this.appComponent.user.id_brand;
    this.model.store = this.store;
  };

  clickStore(store){
    this.store.id_store = store.id_store;
    this.isSelectStore = false;
    this.isFirstNameMenu = true;
    this.isFirstPriceMenu = true;
    this.isFirstKategoriMenu = true;
    this.isFirstBahanBaku = true;
    this.isFirstJumlahBahanBaku = true;
    this.isAddMenu = false;
    this.isEditMenu = false;
    this.isPreviewMenu = false;
    this.isAddDetailMenu = false;
    this.isEditDetailMenu = false;
    this.isPreviewDetailMenu = false;
    this.getMenus(store.id_store);
    this.getBahanBakus(store.id_store);
    this.isShowDetailMenu = false;
    this.detailmenus = [];
  };

  clickKategori(kategori){
    this.isFirstKategoriMenu = true;
    this.kategori.id_kategori_menu = kategori.id_kategori_menu;
    this.kategori.name_kategori_menu = kategori.name_kategori_menu;
  };

  clickAddMenu(){
    if (this.store.id_store == "" || this.store.id_store == undefined){
      this.isSelectStore = true;
    } else {
      this.dataImage = {};
      this.menu = {};
      this.kategori = {};
      this.tempMenuSelected = {};
      this.isFirstNameMenu = true;
      this.isFirstPriceMenu = true;
      this.isFirstKategoriMenu = true;
      this.isEditMenu = false;
      this.isPreviewMenu = false;
      this.isAddMenu = true;
      this.isPhotoDefault = true;
      this.isPhotoImport = false;
      this.isShowDetailMenu = false;
    }
  };

  clickEditMenu(menu){
    this.dataImage = {};
    this.isFirstNameMenu = true;
    this.isFirstPriceMenu = true;
    this.isFirstKategoriMenu = true;
    this.isAddMenu = false;
    this.isPreviewMenu = false;
    this.isEditMenu = true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.menuSelected(menu);
    this.tempMenuSelected = menu;
    this.isShowDetailMenu = true;
    this.isEditDetailMenu = false;
    this.isPreviewDetailMenu = false;
    this.isAddDetailMenu = false;
    this.detail_menu = {};
    this.satuan = {};
    this.isShowDetailMenu = false;
  };

  clickDeleteMenu(menu){
    this.modelDelete.id_brand = this.appComponent.user.id_brand;
    this.modelDelete.id_store = this.store.id_store;
    this.modelDelete.id_menu = menu.id_menu;
    this.displayDeleted('#deletedBG', '#deleted', 'block');
    this.displayComponentDeleted('#ask1', '#ask2', '#yes1', '#yes2', 'block', 'none');
  };

  clickPreviewMenu(menu){
    this.dataImage = {};
    this.isFirstNameMenu = true;
    this.isFirstPriceMenu = true;
    this.isFirstKategoriMenu = true;
    this.isAddMenu = false;
    this.isEditMenu = false;
    this.isPreviewMenu = true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.menuSelected(menu);
    this.tempMenuSelected = menu;
    this.isShowDetailMenu = true;
    this.isEditDetailMenu = false;
    this.isPreviewDetailMenu = false;
    this.isAddDetailMenu = false;
    this.detail_menu = {};
    this.satuan = {};
    this.getDetailMenus(menu);
    this.modelDelete.id_menu  = menu.id_menu;
  };

  clickAddDetailMenu(){
    this.dataImage = {};
    this.isFirstBahanBaku = true;
    this.isFirstJumlahBahanBaku = true;
    this.isEditMenu = false;
    this.isAddMenu = false;
    this.isPreviewMenu = true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.isEditDetailMenu = false;
    this.isPreviewDetailMenu = false;
    this.isAddDetailMenu = true;
    this.menuSelectedByDetailMenu(this.tempMenuSelected);
    this.detail_menu = {};
    this.satuan = {};
  };

  clickEditDetailMenu(detailMenu){
    this.dataImage = {};
    this.isFirstBahanBaku = true;
    this.isFirstJumlahBahanBaku = true;
    this.isAddMenu = false;
    this.isEditMenu = false;
    this.isPreviewMenu = true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.isPreviewDetailMenu = false;
    this.isAddDetailMenu = false;
    this.isEditDetailMenu = true;
    this.menuSelectedByDetailMenu(this.tempMenuSelected);
    this.detailMenuSelected(detailMenu);
  };

  clickDeleteDetailMenu(detailMenu){
    this.modelDelete.id_brand = this.appComponent.user.id_brand;
    this.modelDelete.id_store = this.store.id_store;
    this.modelDelete.id_bahan_baku = detailMenu.id_bahan_baku;
    this.displayDeleted('#deletedBG','#deleted', 'block');
    this.displayComponentDeleted('#ask1', '#ask2', '#yes1', '#yes2', 'none', 'block')
  };

  clickPreviewDetailMenu(detailMenu){
    this.dataImage = {};
    this.isFirstBahanBaku = true;
    this.isFirstJumlahBahanBaku = true;
    this.isAddMenu = false;
    this.isEditMenu = false;
    this.isPreviewMenu = true;
    this.isPhotoDefault = true;
    this.isPhotoImport = false;
    this.isAddDetailMenu = false;
    this.isEditDetailMenu = false;
    this.isPreviewDetailMenu = true;
    this.menuSelectedByDetailMenu(this.tempMenuSelected);
    this.detailMenuSelected(detailMenu);
  };

  menuSelected(menu){
    this.menu.id_menu = menu.id_menu;
    this.menu.name_menu = menu.name_menu;
    this.menu.price = menu.price;
    this.menu.stok = menu.stok;
    this.menu.path_image = menu.path_image;
    this.kategori.id_kategori_menu = menu.kategori.id_kategori_menu;
    this.kategori.name_kategori_menu = menu.kategori.name_kategori_menu;
  };

  menuSelectedByDetailMenu(menu){
    this.isFirstNameMenu = true;
    this.isFirstPriceMenu = true;
    this.isFirstKategoriMenu = true;
    this.menu.id_menu = menu.id_menu;
    this.menu.name_menu = menu.name_menu;
    this.menu.price = menu.price;
    this.menu.stok = menu.stok;
    this.menu.path_image = menu.path_image;
    this.kategori.id_kategori_menu = menu.kategori.id_kategori_menu;
    this.kategori.name_kategori_menu = menu.kategori.name_kategori_menu;
  };

  detailMenuSelected(detailMenu){
    this.detail_menu.id_bahan_baku = detailMenu.id_bahan_baku;
    this.detail_menu.name_bahan_baku = detailMenu.name_bahan_baku;
    this.detail_menu.jumlah = detailMenu.jumlah;
    this.satuan.id_satuan = detailMenu.satuan.id_satuan;
    this.satuan.name_satuan = detailMenu.satuan.name_satuan;
    this.selectedBahanBaku = detailMenu.name_bahan_baku;
  };

  getStores(){
    this.stores = [];
    this.storeService.getStore(this.appComponent.user.id_brand).subscribe(
      data => {
        for(let i=0; i<data.store.length; i++){
          if(data.store[i].id_store != 'store.XXXX' && data.store[i].deleted == false){
            this.stores.push(data.store[i]);
          }
        }
      }, 
      error => {
        console.log(error);
      }
    )
  };

  getKategoris(){
    this.kategoris = [];
    this.kategoriMenuService.getKategori(this.appComponent.user.id_brand).subscribe(
      data =>{
        for (let i=0; i< data.result.kategori_menu.length; i++){
          if (data.result.kategori_menu[i].deleted == false){
            this.kategoris.push(data.result.kategori_menu[i]);
          }
        };
      },
      error =>{
        console.log(error);
      }
    )
  };

  getBahanBakus(idStore){
    this.bahanbakus = [];
    this.bahanBakuService.getBahanBaku(this.appComponent.user.id_brand+'/'+idStore).subscribe(
      data =>{
        for (let i=0; i<data.length; i++){
          if( data[i].bahan_baku.deleted == false){
            this.bahanbakus.push(data[i]);
          }
        }
      }, 
      error =>{
        console.log(error);
      }
    )
  };

  getMenus(idStore){
    this.menus = [];
    this.menuService.getMenu(this.appComponent.user.id_brand+'/'+idStore).subscribe(
      data =>{
        let no= 0;
        for (var i=0; i<data.result.length; i++){
          if(data.result[i].deleted == false){
            this.menuAndNoUrut = {};
            this.menuAndNoUrut.nourut = no+1;
            this.menuAndNoUrut.menu = data.result[i];
            this.menus.push(this.menuAndNoUrut);
            no+=1;
          }
        }
        this.updateStokMenuSelected(this.menus);
      },
      error =>{
        console.log(error);
      }
    )
  };

  updateStokMenuSelected(menus){
    for(var i=0; i < menus.length; i++){
      if(menus[i].menu.id_menu == this.tempMenuSelected.id_menu){
        this.menu.stok = menus[i].menu.stok;
        this.getDetailMenus(menus[i].menu);
        break;
      }
    };
  };

  getDetailMenus(menu){
    this.detailmenus = [];
    let no= 0;
    for (var i=0; i<menu.detail_menu.length; i++){
      if(menu.detail_menu[i].deleted == false && menu.detail_menu[i].id_bahan_baku != 'first'){
        this.detailMenuAndNoUrut = {};
        this.detailMenuAndNoUrut.nourut = no+1;
        this.detailMenuAndNoUrut.detail_menu = menu.detail_menu[i];
        this.detailmenus.push(this.detailMenuAndNoUrut);
        no+= 1;
      }
    }
  };

  defaultisFirstNameMenu(){
    if (this.menu.name_menu == "" && this.isFirstNameMenu == true){
      this.isFirstNameMenu = true;
    } else if (this.menu.name_menu != "" && this.isFirstNameMenu == true){
      this.isFirstNameMenu = false;
    }
  };

  defaultisFirstPriceMenu(){
    if (this.menu.price == "" && this.isFirstPriceMenu == true){
      this.isFirstPriceMenu = true;
    } else if (this.menu.price != "" && this.isFirstPriceMenu == true){
      this.isFirstPriceMenu = false;
    }
  };

  defaultisFirstKategoriMenu(){
    if (this.kategori.name_kategori_menu == "" && this.isFirstKategoriMenu == true){
      this.isFirstKategoriMenu = true;
    } else if (this.kategori.name_kategori_menu  != "" && this.isFirstKategoriMenu == true){
      this.isFirstKategoriMenu = false;
    }
  };

  defaultisFirstBahanBaku(){
    if (this.detail_menu.id_bahan_baku == "" && this.isFirstBahanBaku == true){
      this.isFirstBahanBaku = true;
    } else if (this.detail_menu.id_bahan_baku != "" && this.isFirstBahanBaku == true){
      this.isFirstBahanBaku = false;
    }
  };

  defaultisFirstJumlahBahanBaku(){
    if (this.detail_menu.jumlah == "" && this.isFirstJumlahBahanBaku == true){
      this.isFirstJumlahBahanBaku = true;
    } else if (this.detail_menu.jumlah != "" && this.isFirstJumlahBahanBaku == true){
      this.isFirstJumlahBahanBaku = false;
    }
  };
  
  defaultisButton(){
    if ((this.menu.name_menu == "" || this.menu.name_menu  == undefined) && this.isFirstNameMenu == true ){
      this.isFirstNameMenu = false;
    };
    if ((this.menu.price == "" || this.menu.price  == undefined) && this.isFirstPriceMenu == true ){
      this.isFirstPriceMenu = false;
    };
    if ((this.kategori.name_kategori_menu == "" || this.kategori.name_kategori_menu == undefined) && this.isFirstKategoriMenu == true ){
      this.isFirstKategoriMenu = false;
    };
    if ((this.detail_menu.id_bahan_baku  == "" || this.detail_menu.id_bahan_baku  == undefined) && this.isFirstBahanBaku == true ){
      this.isFirstBahanBaku = false;
    };
    if ((this.detail_menu.jumlah  == "" || this.detail_menu.jumlah == undefined) && this.isFirstJumlahBahanBaku == true ){
      this.isFirstJumlahBahanBaku = false;
    };
  };

  buttonImportImage(status){
    if(status){
      this.isButtonImportImage = true;
    } else {
      this.isButtonImportImage = false;
    }
  };

  changeisPhotoImport(status){
    if (status == true){
      this.isPhotoImport = true;
    } else {
      this.isPhotoImport = false;
    }
  };

  fileChangeListener($event) {
      var image:any = new Image();
      var file:File = $event.target.files[0];
      var myReader:FileReader = new FileReader();
      var that = this;
      myReader.onloadend = function (loadEvent:any) {
          image.src = loadEvent.target.result;
          that.cropper.setImage(image);
      };
      myReader.readAsDataURL(file);
      this.isButtonImportImage = false;
      this.isPhotoDefault = false;
      this.changeisPhotoImport(false);
  };

  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.isFirstBahanBaku = true;
    this.detail_menu.id_bahan_baku = e.item.bahan_baku.id_bahan_baku;
    this.detail_menu.name_bahan_baku = e.item.bahan_baku.name_bahan_baku;
    this.satuan.id_satuan = e.item.bahan_baku.satuan.id_satuan;
    this.satuan.name_satuan = e.item.bahan_baku.satuan.name_satuan;
    this.selectedBahanBaku = e.item.bahan_baku.name_bahan_baku;
  };

  onClick(event){
    if(event.target.id == 'yes1'){
      this.deleteMenu();
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if (event.target.id == 'yes2'){
      this.deleteDetailMenu();
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if (event.target.className == 'no') {
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if (event.target.id == 'tutup') {
      this.displayDeleted('#deletedBG','#deleted','none');
    } else if ( event.target.id == 'hapus' || event.target.className == 'dialog_box'  || event.target.className == 'tittle_dialog_box' ||  
                event.target.className == 'tittle' || event.target.className == 'ask_dialog_box' ||  event.target.id == 'ask1' ||
                event.target.id == 'ask2' || event.target.className == 'left' || event.target.className == 'yes' || 
                event.target.className == 'right' ){
    } else {
      this.displayDeleted('#deletedBG','#deleted','none');
    };
  };

  displayDeleted(id1, id2, action){
    let div = window.document.querySelector(id1);
    div.style.display = action;
    let div2 = window.document.querySelector(id2);
    div2.style.display = action;
  };

  displayComponentDeleted(idAskMenu, idAskDet, idYesMenu, idYesDet, action1, action2){
    let div = window.document.querySelector(idAskMenu);
    div.style.display = action1;
    let div2 = window.document.querySelector(idAskDet);
    div2.style.display = action2;
    let div3 = window.document.querySelector(idYesMenu);
    div3.style.display = action1;
    let div4 = window.document.querySelector(idYesDet);
    div4.style.display = action2;
  };
}
