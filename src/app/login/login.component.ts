import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './../login.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
   isSubmit:Boolean = false;
   isSuccess: Boolean = false;
   isFail:Boolean = false;
   isForbiden: Boolean = false;
   model: any = {};
  constructor(
    private router: Router,
    private loginService: LoginService, 
    private appComponent: AppComponent) { }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
        this.router.navigate(['/']);
    } 
  }

  login(){
    this.loginService.login(this.model).subscribe(
    	data => {
    		this.isSuccess = true;
        localStorage.setItem('currentUser', JSON.stringify({  token: data.token, user: data.user }));
        this.appComponent.user = data.user;
        this.appComponent.brand = data.user.brand;
        this.appComponent.store = data.user.store;
        this.router.navigate(['/']);
    		return true;
    	}, 
    	error => {
    		if(error.status == 403){
    			this.isForbiden = true;
    		} else if (error.status == 404){
          this.isFail = true;
    		}
    	}
    );
    this.isFail = false;
    this.isSuccess = false;
    this.isForbiden = false;
  }
}
