import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class BrandService {

  private url: string = "http://localhost:3001/v1/";

  constructor(private http: Http) { }

  	addBrand(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
      return this.http.post(this.url+"brand",body,options)
         .map((response:Response)=>response.json())
    }

    uploadImageBrandLocal(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
      return this.http.post(this.url+"upload/imagelocal/brand",body,options)
         .map((response:Response)=>response.json())
    }

    updateBrand(data){
	  	let headers = new Headers({ 'Content-Type': 'application/json' });
	    let options = new RequestOptions({ headers: headers });
	    let body = JSON.stringify(data);
	    return this.http.put(this.url+"updateBrand", body, options)
	   		.map((response:Response)=>response.json())
  	}

    updateImageBrand(data){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      let body = JSON.stringify(data);
      return this.http.put(this.url+"updateImageBrand",body,options)
         .map((response:Response)=>response.json())
    }
}
